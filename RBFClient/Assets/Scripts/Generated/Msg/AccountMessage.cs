/*
 * Copyright (c) 2016-present The Limitart Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

using System.Collections.Generic;

/**
 * 账户模块
 * 
 * @author limitart
 *
 */
public class AccountMessage {
	private const byte PACKAGE_ID = 1;

   	/**
	 * 我的玩家信息
	 *
	 * @author limitart
	 *
	 */
	public class MyPlayerInfo : BinaryMeta {

		/**
		 * 服务器当前时间戳
		 */
		public long ServerTime{ get; set; }
		/**
		 * 账户唯一ID
		 */
		public long UniqueID{ get; set; }
		/**
		 * 玩家名字
		 */
		public string Name{ get; set; }
		/**
		 * 玩家头像信息
		 */
		public int Avatar{ get; set; }
	}
   	/**
	 * 静态数据信息
	 *
	 * @author limitart
	 *
	 */
	public class DataInfo : BinaryMeta {

		/**
		 * 静态数据名称
		 */
		public string DataName{ get; set; }
		/**
		 * 静态数据二进制内容
		 */
		public byte[] DataBin{ get; set; }
	}
   	/**
	 * 静态数据列表
	 *
	 * @author limitart
	 *
	 */
	public class DataSets : BinaryMeta {

		/**
		 * 数据二进制列表
		 */
		public List<DataInfo> Datas
        {
            get { return Datas ?? (Datas = new List<DataInfo>()); }
            set
            {
			Datas = value;
            }
        }
	}

	/**
	 * 登陆失败
	 *
	 * @author limitart
	 *
	 */
	public class ResLoginFail : BinaryEnumMessage {

		/**
		 * 用户名为空或超过32个长度
		 */
		public static ResLoginFail USERNAME_EMPTY_OR_GREATER_THAN_32 = new ResLoginFail((byte)0);
		/**
		 * 用户名不能为中文
		 */
		public static ResLoginFail NOT_CHINESE = new ResLoginFail((byte)1);
		/**
		 * 用户名包敏感字
		 */
		public static ResLoginFail USERNAME_ILEAGLE = new ResLoginFail((byte)2);
		/**
		 * 重复登录
		 */
		public static ResLoginFail LOGIN_AGAIN = new ResLoginFail((byte)3);
		/**
		 * 被顶号
		 */
		public static ResLoginFail BE_LOGINED = new ResLoginFail((byte)4);
		/**
		 * 登录失败
		 */
		public static ResLoginFail LOGIN_FAILED = new ResLoginFail((byte)5);

		public ResLoginFail()
		{
		}

		public ResLoginFail(byte code):base(code)
		{
		}

		public override short MessageID() {
			return BinaryMessages.CreateID(PACKAGE_ID, 9);
		}

	}
	/**
	 * 创建玩家失败
	 *
	 * @author limitart
	 *
	 */
	public class ResCreateMyPlayerFail : BinaryEnumMessage {

		/**
		 * 找不到帐号
		 */
		public static ResCreateMyPlayerFail NO_ACCOUNT = new ResCreateMyPlayerFail((byte)0);
		/**
		 * 帐号不属于你
		 */
		public static ResCreateMyPlayerFail ACCOUNT_NOT_BELONG_TO_YOU = new ResCreateMyPlayerFail((byte)1);
		/**
		 * 该帐号并未登录
		 */
		public static ResCreateMyPlayerFail ACCOUNT_NOT_LOGIN = new ResCreateMyPlayerFail((byte)2);
		/**
		 * 已经创建过玩家了
		 */
		public static ResCreateMyPlayerFail ALREADY_CREATED = new ResCreateMyPlayerFail((byte)3);
		/**
		 * 玩家名称过长
		 */
		public static ResCreateMyPlayerFail NAME_TOO_LONG = new ResCreateMyPlayerFail((byte)4);
		/**
		 * 玩家名称重复
		 */
		public static ResCreateMyPlayerFail NAME_DUPLICATED = new ResCreateMyPlayerFail((byte)5);
		/**
		 * 玩家名称存在敏感字
		 */
		public static ResCreateMyPlayerFail NAME_ILEAGLE = new ResCreateMyPlayerFail((byte)6);
		/**
		 * 头像不存在
		 */
		public static ResCreateMyPlayerFail AVATAR_NOT_EXIST = new ResCreateMyPlayerFail((byte)7);

		public ResCreateMyPlayerFail()
		{
		}

		public ResCreateMyPlayerFail(byte code):base(code)
		{
		}

		public override short MessageID() {
			return BinaryMessages.CreateID(PACKAGE_ID, 17);
		}

	}

	/**
	 * 登录
	 *
	 * @author limitart
	 *
	 */
	public class ReqLoginInner : BinaryMessage {

		/**
		 * 帐号
		 */
		public string Username{ get; set; }

		public override short MessageID() {
			return BinaryMessages.CreateID(PACKAGE_ID, 7);
		}

	}
	/**
	 * 登录成功后发送我的玩家信息
	 *
	 * @author limitart
	 *
	 */
	public class ResMyPlayerInfo : BinaryMessage {

		/**
		 * 我的玩家信息(如果为空则玩家要创建新的角色)
		 */
		public MyPlayerInfo MyPlayerInfo{ get; set; }

		public override short MessageID() {
			return BinaryMessages.CreateID(PACKAGE_ID, 11);
		}

	}
	/**
	 * 创建我的玩家
	 *
	 * @author limitart
	 *
	 */
	public class ReqCreateMyPlayer : BinaryMessage {

		/**
		 * 角色名称
		 */
		public string Name{ get; set; }
		/**
		 * 角色头像
		 */
		public int Avatar{ get; set; }

		public override short MessageID() {
			return BinaryMessages.CreateID(PACKAGE_ID, 13);
		}

	}
	/**
	 * 创建我的玩家成功
	 *
	 * @author limitart
	 *
	 */
	public class ResCreateMyPlayerSuccess : BinaryMessage {

		/**
		 * 我的玩家信息(如果为空则玩家要创建新的角色)
		 */
		public MyPlayerInfo MyPlayerInfo{ get; set; }

		public override short MessageID() {
			return BinaryMessages.CreateID(PACKAGE_ID, 15);
		}

	}
	/**
	 * 向服务器发送心跳
	 *
	 * @author limitart
	 *
	 */
	public class ReqAccountHeart : BinaryMessage {


		public override short MessageID() {
			return BinaryMessages.CreateID(PACKAGE_ID, 19);
		}

	}
	/**
	 * 服务器返回心跳
	 *
	 * @author limitart
	 *
	 */
	public class ResAccountHeart : BinaryMessage {

		/**
		 * 服务器当前时间戳
		 */
		public long ServerTime{ get; set; }

		public override short MessageID() {
			return BinaryMessages.CreateID(PACKAGE_ID, 21);
		}

	}
	/**
	 * 向服务器发送心跳
	 *
	 * @author limitart
	 *
	 */
	public class ResDataSet : BinaryMessage {

		/**
		 * 静态数据
		 */
		public byte[] Data{ get; set; }

		public override short MessageID() {
			return BinaryMessages.CreateID(PACKAGE_ID, 23);
		}

	}
}
