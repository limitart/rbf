/** This is an automatically generated class by FairyGUI. Please do not modify it. **/

using FairyGUI;
using FairyGUI.Utils;

namespace Account
{
	public partial class UI_login_btn : GButton
	{
		public Controller m_button;
		public GGraph m_n0;
		public GGraph m_n1;
		public GGraph m_n2;

		public const string URL = "ui://8r2rielhbtv91";

		public static UI_login_btn CreateInstance()
		{
			return (UI_login_btn)UIPackage.CreateObject("Account","login_btn");
		}

		public UI_login_btn()
		{
		}

		public override void ConstructFromXML(XML xml)
		{
			base.ConstructFromXML(xml);

			m_button = this.GetControllerAt(0);
			m_n0 = (GGraph)this.GetChildAt(0);
			m_n1 = (GGraph)this.GetChildAt(1);
			m_n2 = (GGraph)this.GetChildAt(2);
		}
	}
}