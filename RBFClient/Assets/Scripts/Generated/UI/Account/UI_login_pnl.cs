/** This is an automatically generated class by FairyGUI. Please do not modify it. **/

using FairyGUI;
using FairyGUI.Utils;

namespace Account
{
	public partial class UI_login_pnl : GComponent
	{
		public GTextField m_n0;
		public GTextField m_n1;
		public GTextInput m_user_input;
		public GTextField m_n5;
		public GTextInput m_server_input;
		public UI_login_btn m_login_btn;
		public GTextField m_port_input;

		public const string URL = "ui://8r2rielhr33q0";

		public static UI_login_pnl CreateInstance()
		{
			return (UI_login_pnl)UIPackage.CreateObject("Account","login_pnl");
		}

		public UI_login_pnl()
		{
		}

		public override void ConstructFromXML(XML xml)
		{
			base.ConstructFromXML(xml);

			m_n0 = (GTextField)this.GetChildAt(0);
			m_n1 = (GTextField)this.GetChildAt(1);
			m_user_input = (GTextInput)this.GetChildAt(2);
			m_n5 = (GTextField)this.GetChildAt(3);
			m_server_input = (GTextInput)this.GetChildAt(4);
			m_login_btn = (UI_login_btn)this.GetChildAt(5);
			m_port_input = (GTextField)this.GetChildAt(6);
		}
	}
}