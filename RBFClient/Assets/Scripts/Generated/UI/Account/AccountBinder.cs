/** This is an automatically generated class by FairyGUI. Please do not modify it. **/

using FairyGUI;

namespace Account
{
	public class AccountBinder
	{
		public static void BindAll()
		{
			UIObjectFactory.SetPackageItemExtension(UI_login_btn.URL, typeof(UI_login_btn));
			UIObjectFactory.SetPackageItemExtension(UI_login_pnl.URL, typeof(UI_login_pnl));
		}
	}
}