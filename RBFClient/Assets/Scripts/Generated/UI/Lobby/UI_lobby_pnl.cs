/** This is an automatically generated class by FairyGUI. Please do not modify it. **/

using FairyGUI;
using FairyGUI.Utils;

namespace Lobby
{
	public partial class UI_lobby_pnl : GComponent
	{
		public GTextField m_user_txt;
		public GGraph m_avartar_img;

		public const string URL = "ui://vas2plcbog0c0";

		public static UI_lobby_pnl CreateInstance()
		{
			return (UI_lobby_pnl)UIPackage.CreateObject("Lobby","lobby_pnl");
		}

		public UI_lobby_pnl()
		{
		}

		public override void ConstructFromXML(XML xml)
		{
			base.ConstructFromXML(xml);

			m_user_txt = (GTextField)this.GetChildAt(0);
			m_avartar_img = (GGraph)this.GetChildAt(1);
		}
	}
}