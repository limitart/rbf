/** This is an automatically generated class by FairyGUI. Please do not modify it. **/

using FairyGUI;

namespace Lobby
{
	public class LobbyBinder
	{
		public static void BindAll()
		{
			UIObjectFactory.SetPackageItemExtension(UI_lobby_pnl.URL, typeof(UI_lobby_pnl));
		}
	}
}