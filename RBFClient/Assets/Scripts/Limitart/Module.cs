﻿/// <summary>
/// 模块定义
/// </summary>
public interface Module
{
    /// <summary>
    /// 模块初始化
    /// </summary>
    /// <param name="onLoginButtonClick"></param>
    void Init();
    /// <summary>
    /// 帧更新
    /// </summary>
    /// <param name="delta"></param>
    void Update(float delta);
    /// <summary>
    /// 模块销毁
    /// </summary>
    void Destroy();
}
