﻿using System;
using System.Diagnostics;
using System.Reflection;
/// <summary>
/// 日志工厂
/// </summary>
public abstract class Loggers 
{
    private volatile static Loggers DEFAULT;
    private static object locker = new object();

    public  static void SetDefault(Loggers factory)
    {
        lock (locker)
        {
            DEFAULT = factory;
        }
    }

    public static Logger Create()
    {
        StackTrace trace = new StackTrace();
        StackFrame frame = trace.GetFrame(1);
        MethodBase method = frame.GetMethod();
        String className = method.ReflectedType.Name;
        return Create(className);
    }

    public static Logger Create(Type clazz)
    {
        return Create(clazz.Name);
    }

    public static Logger Create(string name)
    {
        if (DEFAULT == null)
        {
            DEFAULT = new UnityLoggers();
        }
        return DEFAULT.Instance(name);
    }

    protected abstract Logger Instance(string name);
}
