﻿using System;
/// <summary>
/// 日志接口
/// </summary>
public interface Logger 
{
    string Name();

    bool IsTraceEnabled();

    void Trace(object msg);

    void Trace(string format, params object[] args);

    void Trace(Exception t);

    bool IsDebugEnabled();

    void Debug(object msg);

    void Debug(string format, params object[] args);

    void Debug(Exception t);

    bool IsInfoEnabled();

    void Info(object msg);

    void Info(string format, params object[] args);

    void Info(Exception t);

    bool IsWarnEnabled();

    void Warn(object msg);

    void Warn(string format, params object[] args);

    void Warn(Exception t);

    bool IsErrorEnabled();

    void Error(object msg);

    void Error(string format, params object[] args);

    void Error(Exception t);

    bool IsEnabled(LogLevel level);

    void Log(LogLevel level, object msg);

    void Log(LogLevel level, string format, params object[] args);

    void Log(LogLevel level, Exception t);
}
