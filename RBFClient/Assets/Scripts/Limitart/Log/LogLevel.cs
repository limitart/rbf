﻿/// <summary>
/// 日志等级
/// </summary>
public enum LogLevel 
{
    TRACE,
    DEBUG,
    INFO,
    WARN,
    ERROR
}
