﻿using System;

public class UnityLogger : AbstractLogger
{
    private static string PATTERN = "[{0:yyyy-MM-dd HH:mm:ss:fff} {1}][2]{3}";
    protected string Build(LogLevel level, string template, params object[] args)
    {
        return string.Format(PATTERN, DateTime.Now, level, Name(), string.Format(template, args));
    }

    public override void Debug(object msg)
    {
        if (IsDebugEnabled())
        {
            UnityEngine.Debug.Log(Build(LogLevel.DEBUG, msg.ToString()));
        }
    }

    public override void Debug(string format, params object[] args)
    {
        if (IsDebugEnabled())
        {
            UnityEngine.Debug.Log(Build(LogLevel.DEBUG, format,args));
        }
    }


    public override void Error(object msg)
    {
        if (IsErrorEnabled())
        {
            UnityEngine.Debug.LogError(Build(LogLevel.ERROR, msg.ToString()));
        }
    }

    public override void Error(string format, params object[] args)
    {
        if (IsErrorEnabled())
        {
            UnityEngine.Debug.LogError(Build(LogLevel.ERROR, format, args));
        }
    }


    public override void Info(object msg)
    {
        if (IsInfoEnabled())
        {
            UnityEngine.Debug.Log(Build(LogLevel.INFO, msg.ToString()));
        }
    }

    public override void Info(string format, params object[] args)
    {
        if (IsInfoEnabled())
        {
            UnityEngine.Debug.Log(Build(LogLevel.INFO, format, args));
        }
    }


    public override bool IsDebugEnabled()
    {
        return true;
    }

    public override bool IsErrorEnabled()
    {
        return true;
    }

    public override bool IsInfoEnabled()
    {
        return true;
    }

    public override bool IsTraceEnabled()
    {
        return true;
    }

    public override bool IsWarnEnabled()
    {
        return true;
    }

    public override void Trace(object msg)
    {
        if (IsTraceEnabled())
        {
            UnityEngine.Debug.Log(Build(LogLevel.TRACE, msg.ToString()));
        }
    }

    public override void Trace(string format, params object[] args)
    {
        if (IsTraceEnabled())
        {
            UnityEngine.Debug.Log(Build(LogLevel.TRACE, format, args));
        }
    }


    public override void Warn(object msg)
    {
        if (IsWarnEnabled())
        {
            UnityEngine.Debug.LogWarning(Build(LogLevel.WARN, msg.ToString()));
        }
    }

    public override void Warn(string format, params object[] args)
    {
        if (IsWarnEnabled())
        {
            UnityEngine.Debug.LogWarning(Build(LogLevel.WARN, format, args));
        }
    }

    public override void Debug(Exception t)
    {
        if (IsDebugEnabled())
        {
            UnityEngine.Debug.LogException(t);
        }
    }

    public override void Error(Exception t)
    {
        if (IsErrorEnabled())
        {
            UnityEngine.Debug.LogException(t);
        }
    }

    public override void Info(Exception t)
    {
        if (IsInfoEnabled())
        {
            UnityEngine.Debug.LogException(t);
        }
    }

    public override void Trace(Exception t)
    {
        if (IsTraceEnabled())
        {
            UnityEngine.Debug.LogException(t);
        }
    }

    public override void Warn(Exception t)
    {
        if (IsWarnEnabled())
        {
            UnityEngine.Debug.LogException(t);
        }
    }

    public UnityLogger(string name) : base(name) { }

}
