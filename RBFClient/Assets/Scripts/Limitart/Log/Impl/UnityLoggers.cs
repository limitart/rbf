﻿/// <summary>
/// U3D日志实现
/// </summary>
public class UnityLoggers : Loggers
{
    protected override Logger Instance(string name)
    {
        return new UnityLogger(name);
    }
}
