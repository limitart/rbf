﻿
using System;
/// <summary>
/// 日志抽象实现
/// </summary>
public abstract class AbstractLogger : Logger
{

    private readonly String name;

    protected AbstractLogger(String name)
    {
        this.name = name;
    }



    public abstract void Debug(object msg);

    public abstract void Debug(string format, params object[] args);
    public abstract void Debug(Exception t);


    public abstract void Error(object msg);

    public abstract void Error(string format, params object[] args);
    public abstract void Error(Exception t);


    public abstract void Info(object msg);

    public abstract void Info(string format, params object[] args);
    public abstract void Info(Exception t);

    public abstract bool IsDebugEnabled();

    public bool IsEnabled(LogLevel level)
    {
        switch (level)
        {
            case LogLevel.TRACE:
                return IsTraceEnabled();
            case LogLevel.DEBUG:
                return IsDebugEnabled();
            case LogLevel.INFO:
                return IsInfoEnabled();
            case LogLevel.WARN:
                return IsWarnEnabled();
            case LogLevel.ERROR:
                return IsErrorEnabled();
            default:
                throw new Exception();
        }
    }

    public abstract bool IsErrorEnabled();

    public abstract bool IsInfoEnabled();

    public abstract bool IsTraceEnabled();

    public abstract bool IsWarnEnabled();

    public void Log(LogLevel level, object msg)
    {
        log0(level, null, msg);
    }

    public void Log(LogLevel level, string format, params object[] args)
    {
        log0(level, null, format, args);
    }

    public void Log(LogLevel level, Exception t)
    {
        log0(level, t, null);
    }

    public string Name()
    {
        return this.name;
    }





    public abstract void Trace(object msg);

    public abstract void Trace(string format, params object[] args);
    public abstract void Trace(Exception t);


    public abstract void Warn(object msg);
    public abstract void Warn(string format, params object[] args);
    public abstract void Warn(Exception t);

    private void log0(LogLevel level, Exception t, object msg, params object[] args)
    {
        switch (level)
        {
            case LogLevel.TRACE:
                if (t != null)
                {
                    if (msg == null)
                    {
                        Trace(t);
                    }
                    else
                    {
                        Trace(msg.ToString(), t);
                    }
                }
                else
                {
                    Trace(msg.ToString(), args);
                }
                break;
            case LogLevel.DEBUG:
                if (t != null)
                {
                    if (msg == null)
                    {
                        Debug(t);
                    }
                    else
                    {
                        Debug(msg.ToString(), t);
                    }
                }
                else
                {
                    Debug(msg.ToString(), args);
                }
                break;
            case LogLevel.INFO:
                if (t != null)
                {
                    if (msg == null)
                    {
                        Info(t);
                    }
                    else
                    {
                        Info(msg.ToString(), t);
                    }
                }
                else
                {
                    Info(msg.ToString(), args);
                }
                break;
            case LogLevel.WARN:
                if (t != null)
                {
                    if (msg == null)
                    {
                        Warn(t);
                    }
                    else
                    {
                        Warn(msg.ToString(), t);
                    }
                }
                else
                {
                    Warn(msg.ToString(), args);
                }
                break;
            case LogLevel.ERROR:
                if (t != null)
                {
                    if (msg == null)
                    {
                        Error(t);
                    }
                    else
                    {
                        Error(msg.ToString(), t);
                    }
                }
                else
                {
                    Error(msg.ToString(), args);
                }
                break;
            default:
                throw new Exception();
        }
    }
}
