﻿using System;
using System.Collections.Generic;
/// <summary>
/// 应用定义
/// </summary>
public abstract class BaseApplication : Module
{
    private static readonly Logger LOGGER = Loggers.Create();
    
    public static Singletons SINGLETONS;
    private Dictionary<Type, Module> modules = new Dictionary<Type, Module>();
    /// <summary>
    /// 初始化所有模块
    /// </summary>
    protected abstract void OnInit();
    protected abstract void OnDestroy();
    public void Destroy()
    {
        foreach (Module m in modules.Values)
        {
            m.Destroy();
            LOGGER.Info("{0} destroyed!",m.GetType().Name);
        }
        OnDestroy();
    }

    public void Init()
    {
        //IOC初始化
        SINGLETONS = Singletons.Start().Search();
        List<Type> result = SINGLETONS.Build();
        foreach (Type t in result)
        {
            if (t.GetInterface(typeof(Module).Name) != null)
            {
                modules.Add(t, (Module) SINGLETONS.GetBean(t));
            }
        }

        //各自模块初始化
        foreach (Module m in modules.Values)
        {
            m.Init();
            LOGGER.Info("{0} initialized!",m.GetType().Name);
        }
        OnInit();
    }

    public void Update(float delta)
    {
        foreach (Module m in modules.Values)
        {
            m.Update(delta);
        }
    }

    /// <summary>
    /// 获取模块实例
    /// </summary>
    /// <typeparam name="T"></typeparam>
    /// <returns></returns>
    public T GetModule<T>() where T : Module
    {
        return (T)modules[typeof(T)];
    }
}
