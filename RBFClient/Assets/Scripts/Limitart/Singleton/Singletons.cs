﻿using System;
using System.Collections.Generic;
using System.Reflection;
using UnityEngine;
/// <summary>
/// 单例依赖注入
/// </summary>
public class Singletons
{
    /// <summary>
    /// 实例集合
    /// </summary>
    private Dictionary<Type, object> instances = new Dictionary<Type, object>();
    private List<Type> types = new List<Type>();
    /// <summary>
    /// 创建一个容器实例
    /// </summary>
    /// <returns></returns>
    public static Singletons Start()
    {
        return new Singletons();
    }
    private Singletons()
    {

    }
    /// <summary>
    /// 初始化项目中所有标记有Singleton的实例
    /// </summary>
    /// <returns></returns>
    public Singletons Search()
    {
      Assembly ass = typeof(Singletons).Assembly;
       Type[] types = ass.GetTypes();
        foreach (Type type in types)
        {
            var attribute = type.GetCustomAttributes(typeof(Singleton), false);
            if (attribute.Length == 0)
            {
                continue;
            }
            if (this.types.Contains(type))
            {
                continue;
            }
            Of(type);
        }
        return this;
    }

    /// <summary>
    /// 注册一个单例
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public Singletons Of<T>()
    {
        Type type = typeof(T);
        return Of(type);
    }
    public Singletons Of(Type type) {
        var attribute = type.GetCustomAttributes(typeof(Singleton), false);
        if (attribute.Length == 0)
        {
            throw new SingletonException(type.Name + " not have a 'Singleton' attribute");
        }
        if (this.types.Contains(type))
        {
            throw new SingletonException(type.Name + " duplicated");
        }
        types.Add(type);
        return this;
    }
    /// <summary>
    /// 构建
    /// </summary>
    public List<Type> Build()
    {
        if (this.types.Count == 0)
        {
            throw new SingletonException("not items to build!");
        }
        foreach (Type type in this.types)
        {
            instances.Add(type, Activator.CreateInstance(type));
        }
        foreach (object obj in instances.Values)
        {
            InjectFields(obj);
        }
        List<Type> result = new List<Type>(this.types);
        this.types = null;
        return result;
    }
    /// <summary>
    /// 为字段注入实例
    /// </summary>
    /// <param name="obj"></param>
    private void InjectFields(object obj)
    {
        Type type = obj.GetType();
        var fields = type.GetFields(BindingFlags.NonPublic | BindingFlags.Public | BindingFlags.Instance);
        foreach (var field in fields)
        {
            var attribute = field.GetCustomAttributes(typeof(Inject), false);
            if (attribute.Length == 0)
            {
                continue;
            }
            if (!instances.ContainsKey(field.FieldType))
            {
                throw new SingletonException("can not find type '" + field.FieldType.Name + "' to inject into type '" + type.Name + "'");
            }
            field.SetValue(obj, instances[field.FieldType]);
        }
    }

    /// <summary>
    /// 获取单例
    /// </summary>
    /// <typeparam name="T"></typeparam>
    /// <returns></returns>
    public T GetBean<T>()
    {
        return (T) GetBean(typeof(T));
    }
    public object GetBean(Type type)
    {
        if (types != null)
        {
            throw new SingletonException("Singletons not build yet!");
        }
        return instances[type];
    }
}
