﻿using System;
/// <summary>
/// 指定为单例的类
/// </summary>
[AttributeUsage(AttributeTargets.Class)]
public class Singleton : Attribute
{

}