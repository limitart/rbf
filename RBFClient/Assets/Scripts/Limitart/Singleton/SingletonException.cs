﻿using System;
/// <summary>
/// 单例注入异常
/// </summary>
public class SingletonException : ApplicationException
{
    public SingletonException() { }

    public SingletonException(string message)
        : base(message)
        { }

    public SingletonException(string message, Exception inner)
        : base(message, inner)
        { }
}