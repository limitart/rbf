﻿
public class BinaryMessageCodecException : System.Exception
{
    public BinaryMessageCodecException(string message) : base(message)
    {
    }
}