﻿
public class BinaryMessageIDDuplicatedException : System.Exception
{
    public BinaryMessageIDDuplicatedException(int id) : base(id + "") { }
}