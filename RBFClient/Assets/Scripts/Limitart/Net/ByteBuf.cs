﻿using System;

public class ByteBuf
{
    private byte[] _bytes;
    private int _readIndex;
    private int _writeIndex;
    private int _readIndexMark;
    private int _writeIndexMark;
    private int _capacity;
    private readonly object lockObj = new object();

    private ByteBuf(int capacity)
    {
        _bytes = new byte[capacity];
        this._capacity = capacity;
    }

    private ByteBuf(byte[] bytes)
    {
        this._bytes = bytes;
        this._capacity = bytes.Length;
    }

    /// <summary>
    ///  Create ByteBuf instance with initial capacity
    /// </summary>
    /// <param name="capacity">initial capacity</param>
    /// <returns>instance of ByteBuf</returns>
    public static ByteBuf Alloc(int capacity)
    {
        return new ByteBuf(capacity);
    }
    public static ByteBuf Alloc()
    {
        return new ByteBuf(256);
    }
    /// <summary>
    /// Create ByteBuf instance with initial byte array
    /// </summary>
    /// <param name="bytes">initial byte array</param>
    /// <returns>instance of ByteBuf</returns>
    public static ByteBuf Alloc(byte[] bytes)
    {
        return new ByteBuf(bytes);
    }

    private int FixLength(int length)
    {
        int n = 2;
        int b = 2;
        while (b < length)
        {
            b = 2 << n;
            n++;
        }
        return b;
    }

    private byte[] Flip(byte[] bytes)
    {
        if (BitConverter.IsLittleEndian)
        {
            Array.Reverse(bytes);
        }
        return bytes;
    }

    private void FixSizeAndReset(int currLen, int futureLen)
    {
        if (futureLen > currLen)
        {
            int size = FixLength(currLen) * 2;
            if (futureLen > size)
            {
                size = FixLength(futureLen) * 2;
            }
            byte[] newbuf = new byte[size];
            Array.Copy(_bytes, 0, newbuf, 0, currLen);
            _bytes = newbuf;
            _capacity = newbuf.Length;
        }
    }

    /// <summary>
    /// Write byte array in ByteBuf
    /// </summary>
    /// <param name="bytes">byte array</param>
    /// <param name="startIndex">where begin</param>
    /// <param name="length">where end</param>
    public void WriteBytes(byte[] bytes, int startIndex, int length)
    {
        lock (lockObj)
        {
            int offset = length - startIndex;
            if (offset <= 0) { return; }
            int total = offset + _writeIndex;
            int len = _bytes.Length;
            FixSizeAndReset(len, total);
            for (int i = _writeIndex, j = startIndex; i < total; i++, j++)
            {
                _bytes[i] = bytes[j];
            }
            _writeIndex = total;
        }
    }

    public void SetBytes(int index, byte[] bytes)
    {
        SetBytes(index, bytes, bytes.Length);
    }

    public void SetBytes(int index, byte[] bytes, int length)
    {
        lock (lockObj)
        {
            if (length <= 0) { return; }
            int len = bytes.Length < length ? bytes.Length : length;
            int total = len + index;
            FixSizeAndReset(_bytes.Length, total);
            for (int i = index, j = 0; i < total && j < len; i++, ++j)
            {
                _bytes[i] = bytes[j];
            }
        }
    }

    /// <summary>
    /// Write byte array in ByteBuf
    /// </summary>
    /// <param name="bytes">byte array</param>
    /// <param name="length">the length of bytes</param>
    public void WriteBytes(byte[] bytes, int length)
    {
        WriteBytes(bytes, 0, length);
    }

    /// <summary>
    /// Write byte array in byteBuf
    /// </summary>
    /// <param name="bytes">byte array</param>
    public void WriteBytes(byte[] bytes)
    {
        WriteBytes(bytes, bytes.Length);
    }

    /// <summary>
    /// Write another ByteBuf in this
    /// </summary>
    /// <param name="buffer">another ByteBuf</param>
    public void Write(ByteBuf buffer)
    {
        if (buffer == null) { return; }
        if (buffer.ReadableBytes() <= 0) { return; }
        WriteBytes(buffer.ToArray());
    }

    /// <summary>
    /// Write short(int16)
    /// </summary>
    /// <param name="value">short value</param>
    public void WriteShort(short value)
    {
        WriteBytes(Flip(BitConverter.GetBytes(value)));
    }

    public void SetShort(int index, short value)
    {
        SetBytes(index, Flip(BitConverter.GetBytes(value)));
    }

    /// <summary>
    /// Write unsigned short(int16)
    /// </summary>
    /// <param name="value">ushort value</param>
    public void WriteUshort(ushort value)
    {
        WriteBytes(Flip(BitConverter.GetBytes(value)));
    }

    /// <summary>
    /// Write int32
    /// </summary>
    /// <param name="value">int value</param>
    public void WriteInt(int value)
    {
        WriteBytes(Flip(BitConverter.GetBytes(value)));
    }

    /// <summary>
    /// Write unsigned int32
    /// </summary>
    /// <param name="value">uint value</param>
    public void WriteUint(uint value)
    {
        WriteBytes(Flip(BitConverter.GetBytes(value)));
    }

    /// <summary>
    /// Write long
    /// </summary>
    /// <param name="value">long value</param>
    public void WriteLong(long value)
    {
        WriteBytes(Flip(BitConverter.GetBytes(value)));
    }

    /// <summary>
    /// write unsigned long
    /// </summary>
    /// <param name="value">ulong value</param>
    public void WriteUlong(ulong value)
    {
        WriteBytes(Flip(BitConverter.GetBytes(value)));
    }

    /// <summary>
    /// Write float
    /// </summary>
    /// <param name="value">float value</param>
    public void WriteFloat(float value)
    {
        WriteBytes(Flip(BitConverter.GetBytes(value)));
    }

    /// <summary>
    /// Write byte
    /// </summary>
    /// <param name="value">byte value</param>
    public void WriteByte(byte value)
    {
        lock (lockObj)
        {
            int afterLen = _writeIndex + 1;
            int len = _bytes.Length;
            FixSizeAndReset(len, afterLen);
            _bytes[_writeIndex] = value;
            _writeIndex = afterLen;
        }
    }
    public void WriteByte(int value)
    {
        WriteByte((byte)value);
    }

    /// <summary>
    /// Write double
    /// </summary>
    /// <param name="value">double value</param>
    public void WriteDouble(double value)
    {
        WriteBytes(Flip(BitConverter.GetBytes(value)));
    }

    public void WriteBoolean(bool value)
    {
        WriteBytes(Flip(BitConverter.GetBytes(value)));
    }

    public void WriteChar(int value)
    {
        WriteShort((short)value);
    }
    /// <summary>
    /// Read a byte
    /// </summary>
    /// <returns>byte value</returns>
    public byte ReadByte()
    {
        byte b = _bytes[_readIndex];
        _readIndex++;
        return b;
    }


    private byte[] Read(int len)
    {
        byte[] bytes = new byte[len];
        Array.Copy(_bytes, _readIndex, bytes, 0, len);
        if (BitConverter.IsLittleEndian)
        {
            Array.Reverse(bytes);
        }
        _readIndex += len;
        return bytes;
    }

    /// <summary>
    /// Read unsigned short(unsigned int16)
    /// </summary>
    /// <returns>ushort value</returns>
    public ushort ReadUshort()
    {
        return BitConverter.ToUInt16(Read(2), 0);
    }

    /// <summary>
    /// Read short(int16)
    /// </summary>
    /// <returns>short value</returns>
    public short ReadShort()
    {
        return BitConverter.ToInt16(Read(2), 0);
    }

    /// <summary>
    /// Read unsigned int
    /// </summary>
    /// <returns>uint value</returns>
    public uint ReadUint()
    {
        return BitConverter.ToUInt32(Read(4), 0);
    }

    /// <summary>
    /// Read int32
    /// </summary>
    /// <returns>int value</returns>
    public int ReadInt()
    {
        return BitConverter.ToInt32(Read(4), 0);
    }

    /// <summary>
    /// Read unsigned long
    /// </summary>
    /// <returns>ulong value</returns>
    public ulong ReadUlong()
    {
        return BitConverter.ToUInt64(Read(8), 0);
    }

    /// <summary>
    /// Read long
    /// </summary>
    /// <returns>long value</returns>
    public long ReadLong()
    {
        return BitConverter.ToInt64(Read(8), 0);
    }

    /// <summary>
    /// Read float
    /// </summary>
    /// <returns>float value</returns>
    public float ReadFloat()
    {
        return BitConverter.ToSingle(Read(4), 0);
    }

    /// <summary>
    /// Read double
    /// </summary>
    /// <returns>double value</returns>
    public double ReadDouble()
    {
        return BitConverter.ToDouble(Read(8), 0);
    }
    public char ReadChar()
    {
        return (char)ReadShort();
    }
    public bool ReadBoolean()
    {
        return BitConverter.ToBoolean(Read(1), 0);
    }
    /// <summary>
    /// Read bytes into byte array buffer
    /// </summary>
    /// <param name="toBytes">byte array buffer</param>
    /// <param name="disstart">where begin</param>
    /// <param name="len">the length</param>
    public void ReadBytes(byte[] toBytes, int disstart, int len)
    {
        int size = disstart + len;
        for (int i = disstart; i < size; i++)
        {
            toBytes[i] = this.ReadByte();
        }
    }

    /// <summary>
    /// Read some bytes to byte array buffer
    /// </summary>
    /// <param name="toBytes">byte array buffer</param>
    public void ReadBytes(byte[] toBytes)
    {
        ReadBytes(toBytes, 0, toBytes.Length);
    }

    /// <summary>
    /// Read this ByteBuf into another ByteBuf
    /// </summary>
    /// <param name="buf">another ByteBuf</param>
    public void Read(ByteBuf buf)
    {
        int len = ReadableBytes();
        byte[] bytes = new byte[len];
        ReadBytes(bytes, 0, len);
        buf.WriteBytes(bytes);
    }

    /// <summary>
    /// Discard bytes read
    /// </summary>
    public void DiscardReadBytes()
    {
        if (_readIndex <= 0) { return; }
        int len = _bytes.Length - _readIndex;
        byte[] newbuf = new byte[len];
        Array.Copy(_bytes, _readIndex, newbuf, 0, len);
        _bytes = newbuf;
        _writeIndex -= _readIndex;
        _readIndexMark -= _readIndex;
        if (_readIndexMark < 0)
        {
            _readIndexMark = _readIndex;
        }
        _writeIndexMark -= _readIndex;
        if (_writeIndexMark < 0 || _writeIndexMark < _readIndex || _writeIndexMark < _readIndexMark)
        {
            _writeIndexMark = _writeIndex;
        }
        _readIndex = 0;
    }

    /// <summary>
    /// Clear ByteBuf
    /// </summary>
    public void Clear()
    {
        _bytes = new byte[_bytes.Length];
        _readIndex = 0;
        _writeIndex = 0;
        _readIndexMark = 0;
        _writeIndexMark = 0;
    }

    private void SetReaderIndex(int index)
    {
        if (index < 0) { return; }
        _readIndex = index;
    }

    /// <summary>
    /// Mark the current reading position
    /// </summary>
    public void MarkReaderIndex()
    {
        _readIndexMark = _readIndex;
    }

    /// <summary>
    ///  Mark the current writting position
    /// </summary>
    public void MarkWriterIndex()
    {
        _writeIndexMark = _writeIndex;
    }

    /// <summary>
    /// Go back to the last location of reading
    /// </summary>
    public void ResetReaderIndex()
    {
        _readIndex = _readIndexMark;
    }

    /// <summary>
    /// Go back to the last location of writting
    /// </summary>
    public void ResetWriterIndex()
    {
        _writeIndex = _writeIndexMark;
    }

    /// <summary>
    /// How much can be read
    /// </summary>
    /// <returns>readable length</returns>
    public int ReadableBytes()
    {
        return _writeIndex - _readIndex;
    }

    /// <summary>
    /// Into an array
    /// </summary>
    /// <returns>copy of the array</returns>
    public byte[] ToArray()
    {
        byte[] bytes = new byte[_writeIndex];
        Array.Copy(_bytes, 0, bytes, 0, bytes.Length);
        return bytes;
    }

    /// <summary>
    /// Get capacity
    /// </summary>
    /// <returns>capacity</returns>
    public int Capacity()
    {
        return this._capacity;
    }

    public bool IsReadable()
    {
        return (this._writeIndex > this._readIndex);
    }
}
