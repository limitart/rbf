﻿using System.Net.Sockets;
using System;

public interface BinaryClientEventListener
{
    void OnExceptionCaght(Exception exception);
    void DispatchMessage(BinaryMessageFactory factory,BinaryMessage message);
    void OnConnected(Socket socket);
}