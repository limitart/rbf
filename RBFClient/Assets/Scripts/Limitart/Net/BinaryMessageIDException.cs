﻿
public class BinaryMessageIDException : System.Exception
{
    public BinaryMessageIDException(int id) : base(id + "") { }
}