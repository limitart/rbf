﻿
public class IllegalAccessError : System.SystemException
{
    public IllegalAccessError(string message) : base(message)
    {
    }
}