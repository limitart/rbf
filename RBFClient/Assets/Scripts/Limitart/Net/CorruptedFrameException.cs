﻿
public class CorruptedFrameException : System.Exception
{
    public CorruptedFrameException(string message) : base(message)
    {
    }
}
