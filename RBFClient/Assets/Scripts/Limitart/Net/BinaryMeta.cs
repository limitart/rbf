﻿using System;
using System.Collections.Generic;
using System.Reflection;
using System.Text;

/// <summary>
/// 二进制元数据
/// hank
/// </summary>
public abstract class BinaryMeta
{
    private const bool COMPRESS_INT = true;
    private ByteBuf buffer;

    public ByteBuf Buffer()
    {
        return this.buffer;
    }

    public void Buffer(ByteBuf buffer)
    {
        this.buffer = buffer;
    }

    public void Encode()
    {
        PropertyInfo[] fields = GetType().GetProperties();
        foreach (PropertyInfo temp in
            fields)
        {
            WriteField(temp);
        }
    }

    public void Decode()
    {
        PropertyInfo[] fields = GetType().GetProperties();
        foreach (PropertyInfo temp in
            fields)
        {
            ReadField(temp);
        }
    }

    private void WriteField(PropertyInfo field)
    {
        var type = field.PropertyType;
        var obj = field.GetValue(this, null);
        if (type.IsPrimitive)
        {
            if (type == typeof(byte))
            {
                PutByte((byte) field.GetValue(this, null));
            }
            else if (type == typeof(short))
            {
                PutShort((short) field.GetValue(this, null));
            }
            else if (type == typeof(int))
            {
                PutInt((int) field.GetValue(this, null));
            }
            else if (type == typeof(long))
            {
                PutLong((long) field.GetValue(this, null));
            }
            else if (type == typeof(float))
            {
                PutFloat((float) field.GetValue(this, null));
            }
            else if (type == typeof(double))
            {
                PutDouble((double) field.GetValue(this, null));
            }
            else if (type == typeof(char))
            {
                PutChar((char) field.GetValue(this, null));
            }
            else if (type == typeof(bool))
            {
                PutBoolean((bool) field.GetValue(this, null));
            }
        }
        else if (type.IsArray)
        {
            var component = type.GetElementType();
            if (component == typeof(byte))
            {
                PutByteArray((byte[]) obj);
            }
            else if (component == typeof(short))
            {
                PutShortArray((short[]) obj);
            }
            else if (component == typeof(int))
            {
                PutIntArray((int[]) obj);
            }
            else if (component == typeof(long))
            {
                PutLongArray((long[]) obj);
            }
            else if (component == typeof(float))
            {
                PutFloatArray((float[]) obj);
            }
            else if (component == typeof(double))
            {
                PutDoubleArray((double[]) obj);
            }
            else if (component == typeof(char))
            {
                PutCharArray((char[]) obj);
            }
            else if (component == typeof(bool))
            {
                PutBooleanArray((bool[]) obj);
            }
            else if (component.BaseType == typeof(BinaryMeta))
            {
                PutMessageMetaArray((BinaryMeta[]) obj);
            }
            else if (component == typeof(string))
            {
                PutStringArray((string[]) obj);
            }
        }
        else if (typeof(List<>).IsAssignableFrom(type))
        {
            var component = type.GetGenericArguments()[0];
            if (component == typeof(byte))
            {
                PutByteList((List<byte>) obj);
            }
            else if (component == typeof(short))
            {
                PutShortList((List<short>) obj);
            }
            else if (component == typeof(int))
            {
                PutIntList((List<int>) obj);
            }
            else if (component == typeof(long))
            {
                PutLongList((List<long>) obj);
            }
            else if (component == typeof(float))
            {
                PutFloatList((List<float>) obj);
            }
            else if (component == typeof(double))
            {
                PutDoubleList((List<double>) obj);
            }
            else if (component == typeof(char))
            {
                PutCharList((List<char>) obj);
            }
            else if (component == typeof(bool))
            {
                PutBooleanList((List<Boolean>) obj);
            }
            else if (component.BaseType == typeof(BinaryMeta))
            {
                PutMessageMetaList((List<BinaryMeta>) obj);
            }
            else if (component == typeof(string))
            {
                PutStringList((List<string>) obj);
            }

            //TODO list基础类型的box类型
        }
        else
        {
            //TODO 基础类型的box类型
            if (type.BaseType == typeof(BinaryMeta))
            {
                BinaryMeta next = (BinaryMeta) obj;
                PutMessageMeta(next);
            }
            else if (type == typeof(string))
            {
                PutString((string) obj);
            }
            else
            {
                throw new BinaryMessageCodecException(GetType()
                                                      + " type error(non MessageMeta field must be primitive(or it's box object),array or List. array's component  and List's generic param as the same as non MessageMeta rule ):"
                                                      + type.Name);
            }
        }
    }

    private void ReadField(PropertyInfo field)
    {
        var type = field.PropertyType;
        if (type.IsPrimitive)
        {
            if (type == typeof(byte))
            {
                field.SetValue(this, GetByte(), null);
            }
            else if (type == typeof(short))
            {
                field.SetValue(this, GetShort(), null);
            }
            else if (type == typeof(int))
            {
                field.SetValue(this, GetInt(), null);
            }
            else if (type == typeof(long))
            {
                field.SetValue(this, GetLong(), null);
            }
            else if (type == typeof(float))
            {
                field.SetValue(this, GetFloat(), null);
            }
            else if (type == typeof(double))
            {
                field.SetValue(this, GetDouble(), null);
            }
            else if (type == typeof(char))
            {
                field.SetValue(this, GetChar(), null);
            }
            else if (type == typeof(bool))
            {
                field.SetValue(this, GetBoolean(), null);
            }
        }
        else if (type.IsArray)
        {
            var component = type.GetElementType();
            if (component == typeof(byte))
            {
                field.SetValue(this, GetByteArray(), null);
            }
            else if (component == typeof(short))
            {
                field.SetValue(this, GetShortArray(), null);
            }
            else if (component == typeof(int))
            {
                field.SetValue(this, GetIntArray(), null);
            }
            else if (component == typeof(long))
            {
                field.SetValue(this, GetLongArray(), null);
            }
            else if (component == typeof(float))
            {
                field.SetValue(this, GetFloatArray(), null);
            }
            else if (component == typeof(double))
            {
                field.SetValue(this, GetDoubleArray(), null);
            }
            else if (component == typeof(char))
            {
                field.SetValue(this, GetCharArray(), null);
            }
            else if (component == typeof(bool))
            {
                field.SetValue(this, GetBooleanArray(), null);
            }
            //TODO list基础类型的box类型
            else if (component.BaseType == typeof(BinaryMeta))
            {
                field.SetValue(this, GetMessageMetaArray(component), null);
            }
            else if (component == typeof(string))
            {
                field.SetValue(this, GetStringArray(), null);
            }
        }
        else if (typeof(List<>).IsAssignableFrom(type))
        {
            var component = type.GetGenericArguments()[0];
            if (component == typeof(byte))
            {
                field.SetValue(this, GetByteList(), null);
            }
            else if (component == typeof(short))
            {
                field.SetValue(this, GetShortList(), null);
            }
            else if (component == typeof(int))
            {
                field.SetValue(this, GetIntList(), null);
            }
            else if (component == typeof(long))
            {
                field.SetValue(this, GetLongList(), null);
            }
            else if (component == typeof(float))
            {
                field.SetValue(this, GetFloatList(), null);
            }
            else if (component == typeof(double))
            {
                field.SetValue(this, GetDoubleList(), null);
            }
            else if (component == typeof(char))
            {
                field.SetValue(this, GetCharList(), null);
            }
            else if (component == typeof(bool))
            {
                field.SetValue(this, GetBooleanList(), null);
            }
            else if (component.BaseType == typeof(BinaryMeta))
            {
                field.SetValue(this, GetMessageMetaList(component), null);
            }
            else if (component == typeof(string))
            {
                field.SetValue(this, GetStringList(), null);
            }
        }
        else
        {
            //TODO 基础类型的box类型
            if (type.BaseType == typeof(BinaryMeta))
            {
                field.SetValue(this, GetMessageMeta(type), null);
            }
            else if (type == typeof(string))
            {
                field.SetValue(this, GetString(), null);
            }
            else
            {
                throw new BinaryMessageCodecException(GetType()
                                                      + " type error(non MessageMeta field must be primitive(or it's box object),array or List. array's component  and List's generic param as the same as non MessageMeta rule ):"
                                                      + type.Name);
            }
        }
    }

    /**
     * 写入二进制元数据
     * 
     * @param buffer
     * @param meta
     * @throws Exception
     */
    protected void PutMessageMeta(BinaryMeta meta)
    {
        if (meta == null)
        {
            PutByte((byte) 0);
        }
        else
        {
            PutByte((byte) 1);
            meta.Buffer(this.buffer);
            meta.Encode();
        }
    }

    /**
     * 读取二进制元数据
     * 
     * @param buffer
     * @param out
     * @throws Exception
     */
    protected BinaryMeta GetMessageMeta(Type type)
    {
        byte len = GetByte();
        if (len == 0)
        {
            return null;
        }

        BinaryMeta newInstance = Activator.CreateInstance(type) as BinaryMeta;
        newInstance.Buffer(this.buffer);
        newInstance.Decode();
        return newInstance;
    }

    /**
     * 写入二进制元数据列表
     * 
     * @param buffer
     * @param value
     * @throws Exception
     */
    protected void PutMessageMetaList(List<BinaryMeta> value)
    {
        if (value == null)
        {
            PutShort(-1);
        }
        else
        {
            PutInt(value.Count);
            foreach (BinaryMeta temp in value)
            {
                PutMessageMeta(temp);
            }
        }
    }

    /**
     * 读取二进制元数据列表
     * 
     * @param buffer
     * @param clazz
     * @return
     * @throws Exception
     */
    protected List<BinaryMeta> GetMessageMetaList(Type t)
    {
        short len = GetShort();
        if (len == -1)
        {
            return null;
        }
        else
        {
            List<BinaryMeta> list = new List<BinaryMeta>();
            for (int i = 0; i < len; ++i)
            {
                BinaryMeta messageMeta = GetMessageMeta(t);
                list.Add(messageMeta);
            }

            return list;
        }
    }

    /**
     * 写入二进制元数据数组
     * 
     * @param buffer
     * @param value
     * @throws Exception
     */
    protected void PutMessageMetaArray(BinaryMeta[] value)
    {
        if (value == null)
        {
            PutShort(-1);
        }
        else
        {
            PutShort((short) value.Length);
            foreach (BinaryMeta t in value)
            {
                PutMessageMeta(t);
            }
        }
    }

    /**
     * 读取二进制元数据数组
     * 
     * @param buffer
     * @return
     * @throws Exception
     */
    protected BinaryMeta[] GetMessageMetaArray(Type type)
    {
        short len = GetShort();
        if (len == -1)
        {
            return null;
        }
        else
        {
            BinaryMeta[] result = Array.CreateInstance(type, len) as BinaryMeta[];
            for (int i = 0; i < len; ++i)
            {
                result[i] = GetMessageMeta(type);
            }

            return result;
        }
    }

    /**
     * 写入String类型
     * 
     * @param buffer
     * @param value
     */
    protected void PutString(string value)
    {
        if (value == null)
        {
            PutByteArray(null);
        }
        else if ("".Equals(value))
        {
            PutByteArray(new byte[0]);
        }
        else
        {
            byte[] bytes = Encoding.UTF8.GetBytes(value);
            PutByteArray(bytes);
        }
    }

    /**
     * 读取String类型
     * 
     * @param buffer
     * @return
     */
    protected string GetString()
    {
        byte[] bytes = GetByteArray();
        if (bytes == null)
        {
            return null;
        }
        else
        {
            return Encoding.UTF8.GetString(bytes);
        }
    }

    /**
     * 写入String列表
     * 
     * @param buffer
     * @param value
     */
    protected void PutStringList(List<string> value)
    {
        if (value == null)
        {
            PutShort(-1);
        }
        else
        {
            PutShort((short) value.Count);
            foreach (string temp in value)
            {
                PutString(temp);
            }
        }
    }

    /**
     * 读取String列表
     * 
     * @param buffer
     * @return
     */
    protected List<string> GetStringList()
    {
        short len = GetShort();
        if (len == -1)
        {
            return null;
        }
        else
        {
            List<string> list = new List<string>();
            for (int i = 0; i < len; ++i)
            {
                list.Add(GetString());
            }

            return list;
        }
    }

    /**
     * 写入字符串数组
     * 
     * @param buffer
     * @param value
     */
    protected void PutStringArray(string[] value)
    {
        if (value == null)
        {
            PutShort(-1);
        }
        else
        {
            PutShort((short) value.Length);
            foreach (string temp in value)
            {
                PutString(temp);
            }
        }
    }

    /**
     * 读取字符串数组
     * 
     * @param buffer
     * @return
     */
    protected string[] GetStringArray()
    {
        short len = GetShort();
        if (len == -1)
        {
            return null;
        }
        else
        {
            string[] result = new string[len];
            for (int i = 0; i < len; ++i)
            {
                result[i] = GetString();
            }

            return result;
        }
    }

    /**
     * 写入long数据
     * 
     * @param buffer
     * @param value
     */
    protected void PutLong(long value)
    {
        if (COMPRESS_INT)
        {
            WriteRawVarint64(value);
        }
        else
        {
            buffer.WriteLong(value);
        }
    }

    private void WriteRawVarint64(long value)
    {
        // Write out an int 7 bits at a time.  The high bit of the byte,
        // when on, tells reader to continue reading more bytes.
        ulong v = (ulong) value; // support negative numbers
        while (v >= 0x80)
        {
            PutByte((byte) (v | 0x80));
            v >>= 7;
        }

        PutByte((byte) v);
    }

    /**
     * 读取long数据
     * 
     * @param buffer
     * @return
     */
    protected long GetLong()
    {
        if (COMPRESS_INT)
        {
            return ReadRawVarint64();
        }
        else
        {
            return buffer.ReadLong();
        }
    }

    protected long ReadRawVarint64()
    {
        // Read out an Int64 7 bits at a time.  The high bit
        // of the byte when on means to continue reading more bytes.
        long count = 0;
        int shift = 0;
        byte b;
        do
        {
            // Check for a corrupted stream.  Read a max of 10 bytes.
            // In a future version, add a DataFormatException.
            if (shift == 10 * 7) // 10 bytes max per Int64, shift += 7
                throw new FormatException("Format_Bad7BitInt64");

            // ReadByte handles end of stream cases for us.
            b = GetByte();
            count |= (long) (b & 0x7F) << shift;
            shift += 7;
        } while ((b & 0x80) != 0);

        return (long) count;
    }

    /**
     * 写入long列表
     * 
     * @param buffer
     * @param value
     */
    protected void PutLongList(List<long> value)
    {
        if (value == null)
        {
            PutShort(-1);
        }
        else
        {
            PutShort((short) value.Count);
            foreach (long temp in value)
            {
                PutLong(temp);
            }
        }
    }

    /**
     * 读取long列表
     * 
     * @param buffer
     * @return
     */
    protected List<long> GetLongList()
    {
        short len = GetShort();
        if (len == -1)
        {
            return null;
        }
        else
        {
            List<long> list = new List<long>();
            for (int i = 0; i < len; ++i)
            {
                list.Add(GetLong());
            }

            return list;
        }
    }

    /**
     * 写入long数组
     * 
     * @param buffer
     * @param value
     */
    protected void PutLongArray(long[] value)
    {
        if (value == null)
        {
            PutShort(-1);
        }
        else
        {
            PutShort((short) value.Length);
            foreach (long temp in value)
            {
                PutLong(temp);
            }
        }
    }

    /**
     * 读取long数组
     * 
     * @param buffer
     * @return
     */
    protected long[] GetLongArray()
    {
        short len = GetShort();
        if (len == -1)
        {
            return null;
        }
        else
        {
            long[] result = new long[len];
            for (int i = 0; i < len; ++i)
            {
                result[i] = GetLong();
            }

            return result;
        }
    }

    /**
     * 写入int数据
     * 
     * @param buffer
     * @param value
     */
    protected void PutInt(int value)
    {
        if (COMPRESS_INT)
        {
            WriteRawVarint32(value);
        }
        else
        {
            buffer.WriteInt(value);
        }
    }

    private void WriteRawVarint32(int value)
    {
        uint v = (uint) value; // support negative numbers
        while (v >= 0x80)
        {
            PutByte((byte) (v | 0x80));
            v >>= 7;
        }

        PutByte((byte) v);
    }

    /**
     * 读取int数据
     * 
     * @param buffer
     * @return
     */
    protected int GetInt()
    {
        if (COMPRESS_INT)
        {
            return ReadRawVarint32();
        }
        else
        {
            return this.buffer.ReadInt();
        }
    }

    private int ReadRawVarint32()
    {
        int count = 0;
        int shift = 0;
        byte b;
        do
        {
            // Check for a corrupted stream.  Read a max of 5 bytes.
            // In a future version, add a DataFormatException.
            if (shift == 5 * 7) // 5 bytes max per Int32, shift += 7
                throw new FormatException("Format_Bad7BitInt32");

            // ReadByte handles end of stream cases for us.
            b = GetByte();
            count |= (b & 0x7F) << shift;
            shift += 7;
        } while ((b & 0x80) != 0);

        return count;
    }

    /**
     * 写入int列表
     * 
     * @param buffer
     * @param value
     */
    protected void PutIntList(List<int> value)
    {
        if (value == null)
        {
            PutShort(-1);
        }
        else
        {
            PutShort((short) value.Count);
            foreach (int temp in value)
            {
                PutInt(temp);
            }
        }
    }

    /**
     * 读取int列表
     * 
     * @param buffer
     * @return
     */
    protected List<int> GetIntList()
    {
        short len = GetShort();
        if (len == -1)
        {
            return null;
        }
        else
        {
            List<int> list = new List<int>();
            for (int i = 0; i < len; ++i)
            {
                list.Add(GetInt());
            }

            return list;
        }
    }

    /**
     * 写入int数组
     * 
     * @param buffer
     * @param value
     */
    protected void PutIntArray(int[] value)
    {
        if (value == null)
        {
            PutShort(-1);
        }
        else
        {
            PutShort((short) value.Length);
            foreach (int temp in value)
            {
                PutInt(temp);
            }
        }
    }

    /**
     * 读取int数组
     * 
     * @param buffer
     * @return
     */
    protected int[] GetIntArray()
    {
        short len = GetShort();
        if (len == -1)
        {
            return null;
        }
        else
        {
            int[] result = new int[len];
            for (int i = 0; i < len; ++i)
            {
                result[i] = GetInt();
            }

            return result;
        }
    }

    /**
     * 写入byte数据
     * 
     * @param buffer
     * @param value
     */
    protected void PutByte(byte value)
    {
        buffer.WriteByte(value);
    }

    /**
     * 读取byte数据
     * 
     * @param buffer
     * @return
     */
    protected byte GetByte()
    {
        return buffer.ReadByte();
    }

    /**
     * 写入byte列表
     *
     * @param value
     */
    protected void PutByteList(List<byte> list)
    {
        if (list == null)
        {
            PutShort(-1);
        }
        else
        {
            PutShort(list.Count);
            foreach (byte temp in list)
            {
                PutByte(temp);
            }
        }
    }

    /**
      * 读取byte列表
      *
      * @return
      */
    public List<byte> GetByteList()
    {
        short len = GetShort();
        switch (len)
        {
            case -1:
                return null;
            case 0:
                return new List<byte>();
            default:
                List<byte> list = new List<byte>();
                for (int i = 0; i < len; ++i)
                {
                    list.Add(GetByte());
                }

                return list;
        }
    }

    /**
     * 写入byte[]列表
     * 
     * @param buffer
     * @param list
     */
    protected void PutByteArrayList(List<byte[]> list)
    {
        if (list == null)
        {
            PutShort(-1);
        }
        else
        {
            PutShort((short) list.Count);
            foreach (byte[] bt in list)
            {
                PutByteArray(bt);
            }
        }
    }

    /**
     * 读取byte[]列表
     * 
     * @param buffer
     * @return
     */
    protected List<byte[]> GetByteArrayList()
    {
        short len = GetShort();
        if (len == -1)
        {
            return null;
        }
        else
        {
            List<byte[]> list = new List<byte[]>();
            list.Add(GetByteArray());
            return list;
        }
    }

    /**
     * 写入byte数组
     * 
     * @param buffer
     * @param bytes
     */
    protected void PutByteArray(byte[] bytes)
    {
        if (bytes == null)
        {
            PutShort(-1);
        }
        else
        {
            PutShort((short) bytes.Length);
            buffer.WriteBytes(bytes);
        }
    }

    /**
     * 读取byte数组
     * 
     * @param buffer
     * @return
     */
    protected byte[] GetByteArray()
    {
        short len = GetShort();
        if (len == -1)
        {
            return null;
        }
        else
        {
            byte[] bytes = new byte[len];
            buffer.ReadBytes(bytes, 0, len);
            return bytes;
        }
    }

    /**
     * 写入bool数据
     * 
     * @param buffer
     * @param value
     */
    protected void PutBoolean(bool value)
    {
        buffer.WriteBoolean(value);
    }

    /**
     * 读取bool数据
     * 
     * @param buffer
     * @return
     */
    protected bool GetBoolean()
    {
        return buffer.ReadBoolean();
    }

    /**
     * 写入bool列表
     * 
     * @param buffer
     * @param value
     */
    protected void PutBooleanList(List<bool> value)
    {
        if (value == null)
        {
            PutShort(-1);
        }
        else
        {
            PutShort((short) value.Count);
            foreach (bool temp in value)
            {
                PutBoolean(temp);
            }
        }
    }

    /**
     * 读取bool列表
     * 
     * @param buffer
     * @return
     */
    protected List<bool> GetBooleanList()
    {
        short len = GetShort();
        if (len == -1)
        {
            return null;
        }
        else
        {
            List<bool> list = new List<bool>();
            for (int i = 0; i < len; ++i)
            {
                list.Add(GetBoolean());
            }

            return list;
        }
    }

    /**
     * 写入bool数组
     * 
     * @param buffer
     * @param value
     */
    protected void PutBooleanArray(bool[] value)
    {
        if (value == null)
        {
            PutShort(-1);
        }
        else
        {
            PutShort((short) value.Length);
            foreach (bool temp in value)
            {
                PutBoolean(temp);
            }
        }
    }

    /**
     * 读取bool数组
     * 
     * @param buffer
     * @return
     */
    protected bool[] GetBooleanArray()
    {
        short len = GetShort();
        if (len == -1)
        {
            return null;
        }
        else
        {
            bool[] result = new bool[len];
            for (int i = 0; i < len; ++i)
            {
                result[i] = GetBoolean();
            }

            return result;
        }
    }

    /**
     * 写入float数据
     * 
     * @param buffer
     * @param value
     */
    protected void PutFloat(float value)
    {
        buffer.WriteFloat(value);
    }

    /**
     * 读取float数据
     * 
     * @param buffer
     * @return
     */
    protected float GetFloat()
    {
        return buffer.ReadFloat();
    }

    /**
     * 写入float列表
     * 
     * @param buffer
     * @param value
     */
    protected void PutFloatList(List<float> value)
    {
        if (value == null)
        {
            PutShort(-1);
        }
        else
        {
            PutShort((short) value.Count);
            foreach (float temp in value)
            {
                PutFloat(temp);
            }
        }
    }

    /**
     * 读取float列表
     * 
     * @param buffer
     * @return
     */
    protected List<float> GetFloatList()
    {
        short len = GetShort();
        if (len == -1)
        {
            return null;
        }
        else
        {
            List<float> list = new List<float>();
            for (int i = 0; i < len; ++i)
            {
                list.Add(GetFloat());
            }

            return list;
        }
    }

    /**
     * 写入float数组
     * 
     * @param buffer
     * @param value
     */
    protected void PutFloatArray(float[] value)
    {
        if (value == null)
        {
            PutShort(-1);
        }
        else
        {
            PutShort((short) value.Length);
            foreach (float temp in value)
            {
                PutFloat(temp);
            }
        }
    }

    /**
     * 读取float数组
     * 
     * @param buffer
     * @return
     */
    protected float[] GetFloatArray()
    {
        short len = GetShort();
        if (len == -1)
        {
            return null;
        }
        else
        {
            float[] result = new float[len];
            for (int i = 0; i < len; ++i)
            {
                result[i] = GetFloat();
            }

            return result;
        }
    }

    /**
     * 写入double数据
     * 
     * @param buffer
     * @param value
     */
    protected void PutDouble(double value)
    {
        buffer.WriteDouble(value);
    }

    /**
     * 读取double数据
     * 
     * @param buffer
     * @return
     */
    protected double GetDouble()
    {
        return buffer.ReadDouble();
    }

    /**
     * 写入double列表
     * 
     * @param buffer
     * @param value
     */
    protected void PutDoubleList(List<double> value)
    {
        if (value == null)
        {
            PutShort(-1);
        }

        else
        {
            PutShort((short) value.Count);
            foreach (double temp in value)
            {
                PutDouble(temp);
            }
        }
    }

    /**
     * 读取double列表
     * 
     * @param buffer
     * @return
     */
    protected List<double> GetDoubleList()
    {
        short len = GetShort();
        if (len == -1)
        {
            return null;
        }
        else
        {
            List<double> list = new List<double>();
            for (int i = 0; i < len; ++i)
            {
                list.Add(GetDouble());
            }

            return list;
        }
    }

    /**
     * 写入double数组
     * 
     * @param buffer
     * @param value
     */
    protected void PutDoubleArray(double[] value)
    {
        if (value == null)
        {
            PutShort(-1);
        }

        else
        {
            PutShort((short) value.Length);
            foreach (double temp in value)
            {
                PutDouble(temp);
            }
        }
    }

    /**
     * 读取double数组
     * 
     * @param buffer
     * @return
     */
    protected double[] GetDoubleArray()
    {
        short len = GetShort();
        if (len == -1)
        {
            return null;
        }
        else
        {
            double[] result = new double[len];
            for (int i = 0; i < len; ++i)
            {
                result[i] = GetDouble();
            }

            return result;
        }
    }

    /**
     * 写入short数据
     * 
     * @param buffer
     * @param value
     */
    protected void PutShort(int value)
    {
        if (COMPRESS_INT)
        {
            WriteRawVarint32(value);
        }
        else
        {
            buffer.WriteShort((short) value);
        }
    }

    /**
     * 读取short数据
     * 
     * @param buffer
     * @return
     */
    protected short GetShort()
    {
        if (COMPRESS_INT)
        {
            return (short) ReadRawVarint32();
        }
        else
        {
            return buffer.ReadShort();
        }
    }

    /**
     * 写入short列表
     * 
     * @param buffer
     * @param value
     */
    protected void PutShortList(List<short> value)
    {
        if (value == null)
        {
            PutShort(-1);
        }
        else
        {
            PutShort((short) value.Count);
            foreach (short temp in value)
            {
                PutShort(temp);
            }
        }
    }

    /**
     * 读取short列表
     * 
     * @param buffer
     * @return
     */
    protected List<short> GetShortList()
    {
        short len = GetShort();
        if (len == -1)
        {
            return null;
        }
        else
        {
            List<short> list = new List<short>();
            for (int i = 0; i < len; ++i)
            {
                list.Add(GetShort());
            }

            return list;
        }
    }

    /**
     * 写入short数组
     * 
     * @param buffer
     * @param value
     */
    protected void PutShortArray(short[] value)
    {
        if (value == null)
        {
            PutShort(-1);
        }
        else
        {
            PutShort((short) value.Length);
            foreach (short temp in value)
            {
                PutShort(temp);
            }
        }
    }

    /**
     * 读取short数组
     * 
     * @param buffer
     * @return
     */
    protected short[] GetShortArray()
    {
        short len = GetShort();
        if (len == -1)
        {
            return null;
        }
        else
        {
            short[] result = new short[len];
            for (int i = 0; i < len; ++i)
            {
                result[i] = GetShort();
            }

            return result;
        }
    }

    /**
     * 写入char数据
     * 
     * @param buffer
     * @param value
     */
    protected void PutChar(char value)
    {
        buffer.WriteChar((int) value);
    }

    /**
     * 读取char数据
     * 
     * @param buffer
     * @return
     */
    protected char GetChar()
    {
        return buffer.ReadChar();
    }

    /**
     * 写入char列表
     * 
     * @param buffer
     * @param value
     */
    protected void PutCharList(List<char> value)
    {
        if (value == null)
        {
            PutShort(-1);
        }
        else
        {
            PutShort((short) value.Count);
            foreach (char temp in value)
            {
                PutChar(temp);
            }
        }
    }

    /**
     * 读取char列表
     * 
     * @param buffer
     * @return
     */
    protected List<char> GetCharList()
    {
        short len = GetShort();
        if (len == -1)
        {
            return null;
        }
        else
        {
            List<char> list = new List<char>();
            for (int i = 0; i < len; ++i)
            {
                list.Add(GetChar());
            }

            return list;
        }
    }

    /**
     * 写入char数组
     * 
     * @param buffer
     * @param value
     */
    protected void PutCharArray(char[] value)
    {
        if (value == null)
        {
            PutShort(-1);
        }
        else
        {
            PutShort((short) value.Length);
            foreach (char temp in value)
            {
                PutChar(temp);
            }
        }
    }

    /**
     * 读取char数组
     * 
     * @param buffer
     * @return
     */
    protected char[] GetCharArray()
    {
        short len = GetShort();
        if (len == -1)
        {
            return null;
        }
        else
        {
            char[] result = new char[len];
            for (int i = 0; i < len; ++i)
            {
                result[i] = GetChar();
            }

            return result;
        }
    }
}