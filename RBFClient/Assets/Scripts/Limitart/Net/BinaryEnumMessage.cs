﻿public abstract class BinaryEnumMessage : BinaryMessage
{
	private byte code;

	protected BinaryEnumMessage()
	{
	}
	protected BinaryEnumMessage(byte code)
	{
		this.code = code;
	}

	public byte Code
	{
		get { return code; }
	}

	protected bool Equals(BinaryEnumMessage other)
	{
		return code == other.code;
	}

	public override bool Equals(object obj)
	{
		if (ReferenceEquals(null, obj)) return false;
		if (ReferenceEquals(this, obj)) return true;
		if (obj.GetType() != this.GetType()) return false;
		return Equals((BinaryEnumMessage) obj);
	}

	public override int GetHashCode()
	{
		return code.GetHashCode();
	}
}
