﻿
public class DecodeException : System.Exception
{
    public DecodeException(string message) : base(message)
    {
    }
}
