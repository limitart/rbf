﻿
public class BinaryMessages
{
    private static Logger LOGGER = Loggers.Create();
    public static string ID2String(short ID)
    {
        return string.Format("0X%04x", ID).ToUpper();
    }

    /**
     * 构造消息ID
     *
     * @param modID
     * @param mID
     * @return
     * @throws BinaryMessageIDException
     */
    public static short CreateID(int modID, int mID)
    {
        if (modID > short.MaxValue || modID < short.MinValue)
        {
            LOGGER.Error("modID error", new BinaryMessageIDException(modID));
        }
        if (mID > short.MaxValue || mID < short.MinValue)
        {
            LOGGER.Error("mID error", new BinaryMessageIDException(modID));
        }
        return (short)(modID << 8 | mID);
    }

    /**
     * 获取模块ID
     *
     * @param messageID
     * @return
     */
    public static byte ModID(short messageID)
    {
        return (byte)(messageID >> 8);
    }

    /**
     * 获取内容ID
     *
     * @param messageID
     * @return
     */
    public static byte ContentID(short messageID)
    {
        return (byte)(messageID & 0X00FF);
    }
}