﻿using System.Collections.Generic;
using System;
using System.Reflection;

public class BinaryMessageFactory
{
    private static readonly Logger LOGGER = Loggers.Create();
    private Dictionary<short, MessageContext> msgs = new Dictionary<short, MessageContext>();
    private FuncDelegates.Func1<object, Type> confirmInstance;
    private BinaryMessageFactory()
    {
    }

    /// <summary>
    /// 创造一个空的消息工厂
    /// </summary>
    /// <returns></returns>
    public static BinaryMessageFactory Empty()
    {
        return new BinaryMessageFactory();
    }

    /// <summary>
    /// 通过扫描包创建消息工厂
    /// </summary>
    /// <param name="confirmInstance"></param>
    /// <returns></returns>
    public static BinaryMessageFactory Create(FuncDelegates.Func1<object,Type> confirmInstance)
    {
        BinaryMessageFactory factory = new BinaryMessageFactory();
        Assembly ass = typeof(BinaryMessageFactory).Assembly;
        Type[] types = ass.GetTypes();
        foreach (Type type in types)
        {
            factory.RegisterManager(type, confirmInstance);
        }
        return factory;
    }


    /// <summary>
    ///  注册一个manager
    /// </summary>
    /// <param name="clazz"></param>
    /// <param name="confirmInstance"></param>
    /// <returns></returns>
    public BinaryMessageFactory RegisterManager(Type clazz, FuncDelegates.Func1<object, Type> confirmInstance)
    {
        if (this.confirmInstance == null)
        {
            this.confirmInstance = confirmInstance;
        }
        var attribute = clazz.GetCustomAttributes(typeof(BinaryManager), false);
        if (attribute == null|| attribute.Length == 0)
        {
            return this;
        }
        // 扫描方法
        MethodInfo[] methods = clazz.GetMethods();
        for (int i = 0; i < methods.Length; i++)
        {
            MethodInfo info = methods[i];
            var mattr = info.GetCustomAttributes(typeof(BinaryHandler), false);
            if (mattr == null || mattr.Length == 0)
            {
                continue;
            }
            if (!info.IsPublic)
            {
                throw new IllegalAccessError("method must be public:" + clazz.Name + "."
                        + info.Name);
            }
            ParameterInfo[] pinfos = info.GetParameters();
            if (pinfos.Length != 1)
            {
                throw new IllegalArgumentException(clazz + "." + info.Name
                        + " params length must be only one");
            }
            ParameterInfo paramType = pinfos[0];
            if (paramType.ParameterType != typeof(BinaryRequestParam))
            {
                throw new IllegalAccessError(clazz.Name + "." + info.Name
                        + " param can only be " + typeof(BinaryRequestParam).Name);
            }
            BinaryHandler handler = mattr[0] as BinaryHandler;
            BinaryMessage messageInstance = Activator.CreateInstance(handler.MessageType) as BinaryMessage;
            short messageID = messageInstance.MessageID();
            if (msgs.ContainsKey(messageID))
            {
                throw new BinaryMessageIDDuplicatedException(messageID);
            }
            MessageContext messageContext = new MessageContext();
            messageContext.managerClazz = clazz;
            messageContext.method = info;
            messageContext.messageClazz = handler.MessageType;
            msgs.Add(messageID, messageContext);
            LOGGER.Info("register msg " + handler.MessageType.Name + " at " + clazz.Name);
        }
        return this;
    }

    /**
     * 执行消息的处理
     *
     * @param session
     * @param msg
     */
    public void invokeMethod(BinaryClient session, BinaryMessage msg)
    {
        short messageID = msg.MessageID();
        if (!msgs.ContainsKey(messageID))
        {
            LOGGER.Error("message empty,id:" + messageID);
            // 消息上下文不存在
            return;
        }
        MessageContext messageContext = msgs[messageID];
        BinaryRequestParam param = new BinaryRequestParam(session, msg);
        messageContext.method.Invoke(confirmInstance(messageContext.managerClazz), new object[] { param });
    }


    /**
     * 根据ID获取一个消息实例
     *
     * @param msgId
     * @return
     * @throws ReflectiveOperationException
     */
    public BinaryMessage MsgInstance(short msgId)
    {
        if (!msgs.ContainsKey(msgId))
        {
            return null;
        }
        MessageContext messageContext = msgs[msgId];
        return Activator.CreateInstance(messageContext.messageClazz) as BinaryMessage;
    }

}
class MessageContext
{
    public Type managerClazz;
    public Type messageClazz;
    public MethodInfo method;
}