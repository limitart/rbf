﻿public class BinaryRequestParam
{
    private readonly BinaryMessage msg;
    private BinaryClient session;
    private object Extra { get; set; }

    public BinaryRequestParam(BinaryClient session, BinaryMessage msg)
    {
        this.session = session;
        this.msg = msg;
    }

    public T Msg<T>() where T : BinaryMessage
    {
        return (T)msg;
    }
    public BinaryClient Session()
    {
        return this.session;
    }

}