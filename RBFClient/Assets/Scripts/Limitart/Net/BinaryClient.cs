﻿using System;
using System.Net;
using System.Net.Sockets;

public class BinaryClient : IDisposable
{
    private IPEndPoint ipEndpoint;
    private int remotePort;
    private readonly BinaryClientEventListener clientEventListener;
    private BinaryMessageFactory messageFactory;
    private bool disposed;
    private byte[] buf = new byte[1024];
    private readonly ByteBuf buffer = ByteBuf.Alloc(1024);
    private Socket clientSocket;
    public delegate void SendMessageListener(BinaryMessage message, bool result, Exception exception);

    public BinaryClient(string remoteIP, int remotePort, BinaryClientEventListener clientEventListener,
    BinaryMessageFactory messageFactory)
    {
        IPAddress ipAddress = IPAddress.Parse(remoteIP);
        ipEndpoint = new IPEndPoint(ipAddress, remotePort);
        this.clientEventListener = Conditions.NotNull(clientEventListener, "listener");
        this.messageFactory = Conditions.NotNull(messageFactory, "messageFactory");
    }

    public void Connect()
    {
        clientSocket = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);
        clientSocket.BeginConnect(ipEndpoint, ar =>
        {
            clientSocket = ar.AsyncState as Socket;
            clientSocket.EndConnect(ar);
            BeginReceiveData();
            this.clientEventListener.OnConnected(clientSocket);

        }, clientSocket);
    }

    ~BinaryClient()
    {
        Dispose(false);
    }
    private void Dispose(bool disposing)
    {
        if (disposed)
        {
            throw new ObjectDisposedException("BinaryClient", "BinaryClient has been disposed before!");
        }
        if (disposing)
        {
            if (clientSocket != null)
            {
                if (clientSocket.Connected)
                {
                    clientSocket.Shutdown(SocketShutdown.Both);
                }
                clientSocket.Close();
            }
            clientSocket = null;
        }
        disposed = true;
    }
    public void Dispose()
    {
        Dispose(true);
        GC.SuppressFinalize(this);
    }

    public bool IsConnected()
    {
        if (clientSocket == null)
        {
            return false;
        }
        if (!clientSocket.Connected)
        {
            Dispose();
            return false;
        }
        return clientSocket.Connected;
    }

    public void SendMessage(BinaryMessage msg)
    {
        SendMessage(msg, null);

    }
    public void SendMessage(BinaryMessage message, SendMessageListener listener)
    {
        if (IsConnected())
        {
            ByteBuf bufTemp = ByteBuf.Alloc(256);
            bufTemp.WriteShort(0);
            bufTemp.WriteShort(message.MessageID());
            message.Buffer(bufTemp);
            message.Encode();
            bufTemp.SetShort(0, (short)(bufTemp.ReadableBytes() - 2));
            byte[] data = bufTemp.ToArray();
            clientSocket.BeginSend(data, 0, data.Length, SocketFlags.None, asyncResult =>
            {
                clientSocket.EndSend(asyncResult);
                if (listener != null)
                {
                    listener(message, asyncResult.IsCompleted, null);
                }

            }, null);
        }
        else
        {
            if (listener != null)
            {
                listener(message, false, new DecodeException("disconnected!"));
            }
        }
    }

    private void BeginReceiveData()
    {
        if (IsConnected())
        {
            clientSocket.BeginReceive(buf, 0, buf.Length, SocketFlags.None, asyncResult =>
            {
                int count = clientSocket.EndReceive(asyncResult);
                BeginReceiveData();
                buffer.WriteBytes(buf, count);
                Decode();
            }, null);
        }
    }

    private void Decode()
    {
        if (!IsConnected()) { return; }
        int readableBytes = buffer.ReadableBytes();
        if (readableBytes < 2)
        {
            return;
        }
        buffer.MarkReaderIndex();
        short length = buffer.ReadShort();
        if (length < 1)
        {
            buffer.ResetReaderIndex();
            return;
        }
        int afterHeadLength = buffer.ReadableBytes();
        if (afterHeadLength < length)
        {
            buffer.ResetReaderIndex();
            return;
        }
        short messageId = buffer.ReadShort();
        ByteBuf body = ByteBuf.Alloc(length);
        try
        {
            buffer.Read(body);
        }
        catch (Exception e)
        {
            clientEventListener.OnExceptionCaght(e);
            buffer.Clear();
            return;
        }
        buffer.DiscardReadBytes();
        BinaryMessage msg = this.messageFactory.MsgInstance(messageId);
        if (msg == null)
        {
            buffer.Clear();
            clientEventListener.OnExceptionCaght(new DecodeException("message empty,id:" + messageId));
            return;
        }
        msg.Buffer(body);
        msg.Decode();
        this.clientEventListener.DispatchMessage(messageFactory, msg);
    }
}
