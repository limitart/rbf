﻿using System;

/// <summary>
/// 消息的处理方法
/// </summary>
[AttributeUsage(AttributeTargets.Method)]
public class BinaryHandler : Attribute
{
    public Type MessageType { get; set; }
    public BinaryHandler(Type MessageType)
    {
        this.MessageType = MessageType;
    }
}
