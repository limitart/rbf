﻿public class NotSameThreadException : System.Exception
{
    public NotSameThreadException(string message) : base(message)
    {
    }
}
