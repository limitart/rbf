﻿using System;

static class Conditions
{
 /// <summary>
 /// 检测是否为正数
 /// </summary>
 /// <param name="value"></param>
    public static void Positive(int value) {
        Conditions.Args(value > 0, "value must > 0");
    }

    /// <summary>
    /// 检测是否为自然数
    /// </summary>
    /// <param name="value"></param>
    public static void Natural(int value) {
        Conditions.Args(value >= 0, "value must >= 0");
    }

    /// <summary>
    /// 检查元素是否为空，否则抛异常
    /// </summary>
    /// <returns></returns>
    public static  T NotNull<T>(T obj) {
        if (obj == null)
            throw new NullReferenceException();
        return obj;
    }

    /// <summary>
    /// 检查元素是否为空，否则抛异常
    /// </summary>
    /// <returns></returns>
    public static  T NotNull<T>(T obj, string template,params object[] param) {
        if (obj == null)
            throw new NullReferenceException(string.Format(template, param));
        return obj;
    }

    /// <summary>
    /// 检查参数是否正确，否则抛异常
    /// </summary>
    /// <param name="isRight"></param>
    /// <exception cref="IllegalArgumentException"></exception>
    public static void Args(bool isRight) {
        if (!isRight) {
            throw new IllegalArgumentException("");
        }
    }

    /**
     * 检查参数是否正确，否则抛异常
     *
     * @param isRight
     * @param template
     * @param params
     */
    public static void Args(bool isRight, string template, params object[] param) {
        if (!isRight) {
            throw new IllegalArgumentException(string.Format(template,param));
        }
    }

    /**
     * 检查数组边界
     *
     * @param index
     * @param size
     */
    public static int EleIndex(int index, int size) {
        if (index < 0 || index >= size) {
            throw new IndexOutOfRangeException("index:" + index + ",size:" + size);
        }
        return index;
    }

    /**
     * 检查数组边界
     *
     * @param index
     * @param size
     * @param template
     * @param params
     */
    public static int EleIndex(int index, int size, string template, params object[] param) {
        if (index < 0 || index >= size) {
            throw new IndexOutOfRangeException(string.Format(template, param));
        }
        return index;
    }
}
