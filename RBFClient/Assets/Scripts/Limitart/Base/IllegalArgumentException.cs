﻿public class IllegalArgumentException : System.Exception
{
    public IllegalArgumentException(string message) : base(message)
    {
    }
}
