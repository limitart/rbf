﻿
public class FuncDelegates
{
    public delegate R Func<R>();
    public delegate R Func1<R, T>(T t);

    public delegate void Proc();
    public delegate void Proc1<T>(T t);

    public delegate bool Test();
    public delegate bool Test1<T>(T t);
}