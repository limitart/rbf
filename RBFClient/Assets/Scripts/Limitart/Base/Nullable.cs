using System;
 
 /// <summary>
 /// 可能为空
 /// </summary>
 [AttributeUsage(AttributeTargets.Field|AttributeTargets.ReturnValue|AttributeTargets.Parameter|AttributeTargets.Method)]
 public class Nullable : Attribute
 {
 
 }