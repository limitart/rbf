using System.IO;
using System.IO.Compression;

public sealed class CodecUtil
{
    public static byte[] ToGZip(byte[] source)
    {
        MemoryStream ms = new MemoryStream();
        GZipStream gzip = new GZipStream(ms, CompressionMode.Compress, true);
        gzip.Write(source, 0, source.Length);
        gzip.Close();
        return ms.ToArray();
    }

    public static byte[] FromGZip(byte[] source)
    {
        MemoryStream ms = new MemoryStream(source);
        GZipStream gzip = new GZipStream(ms, CompressionMode.Decompress);
        MemoryStream outBuffer = new MemoryStream();
        byte[] buffer = new byte[1024];
        int flag = 0;
        while ((flag = gzip.Read(buffer, 0, buffer.Length)) > 0)
        {
            outBuffer.Write(buffer, 0, flag);
        }

        gzip.Close();
        return outBuffer.ToArray();
    }
}