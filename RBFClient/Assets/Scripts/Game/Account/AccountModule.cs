using System;
using UnityEngine;
using UnityEngine.SceneManagement;

/// <summary>
/// 账户模块
/// </summary>
[Singleton]
[BinaryManager]
public class AccountModule : Module
{
    private static Logger LOGGER = Loggers.Create();
    private long serverTime = 0;
    [Inject] private NetModule _netModule;
    [Inject] private Player _player;
    [Inject] private AccountUI _accountUi;
    [Inject] private LobbyUI _lobbyUi;

    private string user;

    public void Init()
    {
    }

    public void Update(float delta)
    {
    }

    public void Destroy()
    {
    }

    /// <summary>
    /// 请求登录
    /// </summary>
    /// <param name="server"></param>
    /// <param name="user"></param>
    public void reqLogin(string server, int port, string user)
    {
        this.user = user;
        //先链接服务器
        _netModule.ConnectServer(server, port);
    }
    /// <summary>
    /// 登录失败
    /// </summary>
    /// <param name="param"></param>
    [BinaryHandler(typeof(AccountMessage.ResLoginFail))]
    public void OnLoginFail(BinaryRequestParam param)
    {
        AccountMessage.ResLoginFail msg = param.Msg<AccountMessage.ResLoginFail>();
        if (msg == AccountMessage.ResLoginFail.BE_LOGINED)
        {
        }else if (msg == AccountMessage.ResLoginFail.LOGIN_AGAIN)
        {
        }else if (msg == AccountMessage.ResLoginFail.NOT_CHINESE)
        {
        }else if (msg == AccountMessage.ResLoginFail.LOGIN_FAILED)
        {
        }else if (msg == AccountMessage.ResLoginFail.USERNAME_ILEAGLE)
        {
        }else if (msg == AccountMessage.ResLoginFail.USERNAME_EMPTY_OR_GREATER_THAN_32)
        {
        }

        LOGGER.Info("登录帐号{0}失败", msg.Code);
    }
    [BinaryHandler(typeof(AccountMessage.ResMyPlayerInfo))]
    public void OnLoginSuccess(BinaryRequestParam param)
    {
        //销毁登录界面
        _accountUi.closeLoginUI();
        AccountMessage.ResMyPlayerInfo msg = param.Msg<AccountMessage.ResMyPlayerInfo>();
        AccountMessage.MyPlayerInfo accountInfo = msg.MyPlayerInfo;
        if (accountInfo == null)
        {
            //TODO 创建角色
            LOGGER.Info("登录帐号{0}成功，需要创建角色", this.user);
        }
        else
        {
            //TODO 进入大厅
            updateAccountInfo(accountInfo);
            //打开大厅界面
            _lobbyUi.showLobbyUI();
        }
   
    }
    [BinaryHandler(typeof(AccountMessage.ResCreateMyPlayerSuccess))]
    public void onCreatePlayerSuccess(BinaryRequestParam param)
    {
        AccountMessage.ResCreateMyPlayerSuccess msg = param.Msg<AccountMessage.ResCreateMyPlayerSuccess>();
        updateAccountInfo(msg.MyPlayerInfo);
        //打开大厅界面
        _lobbyUi.showLobbyUI();
    }

    public void updateAccountInfo( AccountMessage.MyPlayerInfo accountInfo)
    {
        _player.UniqueID = accountInfo.UniqueID;
        _player.ServerTime = accountInfo.ServerTime;
    }

    public void OnConnected()
    {
        LOGGER.Info("连接服务器成功，开始登录帐号...");
        AccountMessage.ReqLoginInner msg = new AccountMessage.ReqLoginInner();
        msg.Username = this.user;
        _netModule.SendMsg(msg);
    }
}