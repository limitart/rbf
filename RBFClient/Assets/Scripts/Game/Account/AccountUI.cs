﻿using Account;
using FairyGUI;

/// <summary>
/// 登录UI
/// </summary>
[Singleton]
public class AccountUI : Module
{
	private static Logger LOGGER = Loggers.Create();
	private const string PACKAGE="UI/Account";
	private UI_login_pnl _loginPnl;
	[Inject]
	private AccountModule _accountModule;
	public void Init()
	{
		//初始化登录UI
		UIPackage.AddPackage(PACKAGE);
		AccountBinder.BindAll();
		_loginPnl = UI_login_pnl.CreateInstance();
		GRoot.inst.AddChild(_loginPnl);
		//登录按钮添加监听事件
		_loginPnl.m_login_btn.onClick.Add(()=> { OnLoginButtonClick(); });
	}

	private void OnLoginButtonClick()
	{
		LOGGER.Info("点击登录按钮");
		string server = _loginPnl.m_server_input.text;
		int port = int.Parse(_loginPnl.m_port_input.text);
		string user = _loginPnl.m_user_input.text;
		_accountModule.reqLogin(server,port,user);
	}

	public void closeLoginUI()
	{
		if (_loginPnl!=null)
		{
			//销毁界面
			_loginPnl.Dispose();
			//写在UI包
			UIPackage.RemovePackage(PACKAGE);
		}
	}

	public void Update(float delta)
	{
	}

	public void Destroy()
	{
	}
}
