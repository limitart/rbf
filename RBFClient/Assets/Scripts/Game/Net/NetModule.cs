﻿using System;
using System.Collections.Generic;
using System.Net.Sockets;

/// <summary>
/// 网络模块
/// </summary>
[Singleton]
public class NetModule : Module, BinaryClientEventListener
{
    private static Logger LOGGER = Loggers.Create();
    BinaryClient binaryClient;
    private Queue<BinaryMessage> msgs = new Queue<BinaryMessage>();
    private object msgQueueLocker = new object();
    private BinaryMessageFactory messageFactory;
    [Inject] private AccountModule _accountModule;
    [Inject] private DataConfig _dataConfig;

    /// <summary>
    /// 连接到服务器
    /// </summary>
    /// <param name="address"></param>
    public void ConnectServer(string address, int port)
    {
        Conditions.Args(binaryClient == null, "connection already established!");
        messageFactory = BinaryMessageFactory.Create(Confirm);
        binaryClient = new BinaryClient(address, port, this, messageFactory);
        binaryClient.Connect();
    }

    public void SendMsg(BinaryMessage message)
    {
        Conditions.NotNull(binaryClient, "connection not establish!");
        binaryClient.SendMessage(message);
    }

    private BinaryMessage getMsg()
    {
        if (msgs.Count == 0)
        {
            return null;
        }

        lock (msgQueueLocker)
        {
            if (msgs.Count != 0)
            {
                return msgs.Dequeue();
            }
        }

        return null;
    }

    private void putMsg(BinaryMessage msg)
    {
        if (msgs.Count > 100)
        {
            throw new Exception("too many msgs!!!!");
        }

        lock (msgQueueLocker)
        {
            msgs.Enqueue(msg);
        }
    }

    public void Destroy()
    {
        //销毁网络链接
        if (binaryClient != null)
        {
            binaryClient.Dispose();
        }
    }

    public void DispatchMessage(BinaryMessageFactory factory, BinaryMessage message)
    {
        putMsg(message);
    }

    public void Init()
    {
    }

    public object Confirm(Type type)
    {
        return BaseApplication.SINGLETONS.GetBean(type);
    }

    public void OnConnected(Socket socket)
    {
        _accountModule.OnConnected();
    }


    public void OnExceptionCaght(Exception exception)
    {
        LOGGER.Error(exception);
    }

    public void Update(float delta)
    {
        BinaryMessage msg = null;
        while ((msg = getMsg()) != null)
        {
            if (msg.GetType() == typeof(AccountMessage.ResDataSet))
            {
                AccountMessage.ResDataSet dataSet = (AccountMessage.ResDataSet) msg;
                byte[] dataSetData = dataSet.Data;
                _dataConfig.parseDataSet(dataSetData);
                continue;
            }

            messageFactory.invokeMethod(binaryClient, msg);
        }
    }
}