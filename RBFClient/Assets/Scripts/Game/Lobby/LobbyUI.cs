
using FairyGUI;
using Lobby;

/// <summary>
/// 大厅UI
/// </summary>
[Singleton]
public class LobbyUI:Module
{
    private const string PACKAGE = "UI/Lobby";
    [Inject] private Player _player;
    private UI_lobby_pnl _uiLobbyPnl;

    public void showLobbyUI()
    {
        if (_uiLobbyPnl != null)
        {
            return;
        }
        _uiLobbyPnl = UI_lobby_pnl.CreateInstance();
        //设置玩家姓名
        _uiLobbyPnl.m_user_txt.text = _player.user;
        GRoot.inst.AddChild(_uiLobbyPnl);
    }

    public void Init()
    {
        UIPackage.AddPackage(PACKAGE);
        LobbyBinder.BindAll();
    }

    public void Update(float delta)
    {
    }

    public void Destroy()
    {
    }
}