/// <summary>
/// 主玩家信息
/// </summary>
[Singleton]
public class Player
{
    public long ServerTime { set; get; }

    /// <summary>
    /// 玩家唯一ID
    /// </summary>
    public long UniqueID { set; get; }

    /// <summary>
    /// 帐号
    /// </summary>
    public string user { set; get; }
}