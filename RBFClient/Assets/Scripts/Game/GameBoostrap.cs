﻿using UnityEngine;
/// <summary>
/// 游戏入口
/// </summary>
public class GameBoostrap : MonoBehaviour
{

    // Use this for initialization
    void Start()
    {
        GameApplication.INSTANCE.Init();
    }

    // Update is called once per frame
    void Update()
    {
        GameApplication.INSTANCE.Update(Time.deltaTime);
    }
    private void OnApplicationQuit()
    {
        GameApplication.INSTANCE.Destroy();
    }
}
