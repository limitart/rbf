﻿using UnityEngine;
/// <summary>
/// 游戏入口
/// </summary>
public class GameApplication : BaseApplication
{
    private static Logger LOGGER = Loggers.Create();
    /// <summary>
    /// 主体实例
    /// </summary>
    public static GameApplication INSTANCE = new GameApplication();

    protected override void OnDestroy()
    {
        LOGGER.Debug("GameApplication destroy");

    }

    protected override void OnInit()
    {
        LOGGER.Debug("GameApplication init");
    }
}
