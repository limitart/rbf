    using System;
    using System.Collections.Generic;
    using UnityEditor;

public class DataSet
    {
       private static Logger LOGGER = Loggers.Create(typeof(DataSet));
    private Dictionary<Type, DataContainer<DataMeta>> dats = new Dictionary<Type, DataContainer<DataMeta>>();

    public DataSet() {
    }

    public   DataContainer<T> getContainer<T>(Type beanClass) where T:DataMeta
    {
        if (this.dats.ContainsKey(beanClass)) return this.dats[beanClass];
        return null;
    }


    public  DataSet Load<T extends DataMeta, R extends T>(Class<T> type, InputStream inputStream, FuncDelegates.Func1<T, R> reviser) {
        return this.load(type, FileUtil.inputStream2ByteArray(inputStream), reviser);
    }

    public <T extends DataMeta, R extends T> DataSet load(Class<T> type, byte[] bytes, FuncDelegates.Func1<T, R> reviser){
        Conditions.args(!this.dats.containsKey(type), "type {} duplicated", new Object[]{type.getName()});
        List<T> dataMetas = Dats.readDatBin(type, bytes);
        Iterator var5 = dataMetas.iterator();

        while(var5.hasNext()) {
            T dataMeta = (DataMeta)var5.next();
            this.load(dataMeta, reviser);
        }

        LOGGER.info("load data {} success!", type.getName());
        return this;
    }

    public void ForEach(Action<> consumer) {
        this.dats.keySet().forEach(consumer);
    }

    public  DataSet Load<T, R>(T t, FuncDelegates.Func1<T> reviser) where T : DataMeta,R:T {
        Class<? extends DataMeta> aClass = t.getClass();
        this.dats.putIfAbsent(aClass, new DataContainer());
        DataContainer container = (DataContainer)this.dats.get(aClass);
        Field primaryField = t.getClass().getDeclaredFields()[0];
        primaryField.setAccessible(true);
        Object p = primaryField.get(t);
        container.putIfAbsent(p, (DataMeta)Conditions.notNull(reviser.run(t)));
        return this;
    }
    }
