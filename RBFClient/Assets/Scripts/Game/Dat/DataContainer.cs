using System;
using System.Collections.Generic;

/// <summary>
/// 数据容器
/// </summary>
/// <typeparam name="T"></typeparam>
public class DataContainer<T> where T : DataMeta
{
    private List<T> list = new List<T>();
    private Dictionary<object, T> map = new Dictionary<object, T>();

    public DataContainer()
    {
    }

    public void PutIfAbsent(object p, [NotNull] T v)
    {
        Conditions.Args(!this.map.ContainsKey(p), "{},primary key duplicated：%s", v.GetType(), p);
        this.list.Add(v);
        this.map.Add(p, v);
    }

    public void ForEach(Action<T> action)
    {
        this.list.ForEach(action);
    }

    public List<T> CopyList()
    {
        return new List<T>(this.list);
    }

    [Nullable]
    public T Get([NotNull] object primary)
    {
        return this.map[primary];
    }
}