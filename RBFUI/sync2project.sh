#!/bin/bash
rm -rf ../RBFClient/Assets/Resources/UI/*_fui.bytes
rm -rf ../RBFClient/Assets/Scripts/Generated/UI/*

for file in  ./target/*_fui.*;
do 
   echo ${file}
   cp ${file} ../RBFClient/Assets/Resources/UI/
done

cs_dir=./target/*;

for file_name in $cs_dir
do
        if [ -d $file_name ];then
	    echo $file_name
	    cp -r ${file_name} ../RBFClient/Assets/Scripts/Generated/UI/
        fi
done