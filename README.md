# 大逃杀

#### 项目介绍
一个简单的战旗式的大逃杀游戏

#### 软件架构
##### 客户端
  客户端采用Unity作为主体的方式来开发，通信用Limitart的自定义二进制作为核心，为TCP长链接。UI使用FairyGUI作为核心库来开发。FairyGUI在制作的时候请用中文命名文件，用英文命名包，驼峰命名。导出请用二进制格式，并选择自动生成代码。二进制UI文件放入Resources下的UI文件夹里，自动生成的代码放入Generated的UI文件夹里。项目开发尽量分为MVC的形式，控制数据和网络通信流转的为Module，控制UI的以UI结尾，比如帐号模块，数据流转的为AccountModule，UI的脚AccountUI，所有Module均为单例，用Limitart的Singletons来管理，以依赖注入的方式引用。
##### 服务器
  服务器完全以Limitart库来开发，//TODO

#### 参与贡献

1. 联系参与本项目
2. 新建 Feat_xxx 分支
3. 提交代码
4. 新建 Pull Request