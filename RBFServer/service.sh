#!/bin/bash

. /etc/rc.d/init.d/functions

MEM=2g
JAR=`pwd`/game.jar
PID=`ps -ef | grep ${JAR} | grep -v grep | awk '{print $2}'`

exists()
{
	PID=`ps -ef | grep ${JAR} | grep -v grep | awk '{print $2}'`
		if [ ${#PID} -ne 0 ]; then
			return 1
		fi
	return 0
}

start()
{
	echo "startting"
	exists
	if [ $? -ne 0 ]; then
		action $"failed $prog: " /bin/false
		return 1
	fi

	JAVA_PARAM="-Dio.netty.allocator.type=pooled"
	JAVA_PARAM="${JAVA_PARAM} -Dio.netty.leakDetectionLevel=SIMPLE"
	JAVA_PARAM="${JAVA_PARAM} -Dfile.encoding=UTF-8"
	JAVA_PARAM="${JAVA_PARAM} -Dsun.jnu.encoding=UTF-8"
	JAVA_PARAM="${JAVA_PARAM} -Djava.net.preferIPv4Stack=true"
	JAVA_PARAM="${JAVA_PARAM} -XX:+UseBiasedLocking"
	JAVA_PARAM="${JAVA_PARAM} -XX:+PrintGCDetails"
	JAVA_PARAM="${JAVA_PARAM} -Xloggc:gc.log"
	JAVA_PARAM="${JAVA_PARAM} -XX:+HeapDumpOnOutOfMemoryError"
	JAVA_PARAM="${JAVA_PARAM} -server"
	JAVA_PARAM="${JAVA_PARAM} -jar"
	JAVA_PARAM="${JAVA_PARAM} -XX:-OmitStackTraceInFastThrow"
	JAVA_PARAM="${JAVA_PARAM} -XX:+AggressiveOpts"

	JAVA_PARAM="${JAVA_PARAM} -XX:NativeMemoryTracking=summary"

	JAVA_PARAM="${JAVA_PARAM} -Xmx${MEM}"
	JAVA_PARAM="${JAVA_PARAM} -Xms${MEM}"

	JAVA_PARAM="${JAVA_PARAM} -XX:+UseG1GC"
	JAVA_PARAM="${JAVA_PARAM} -XX:MaxMetaspaceSize=256m"
	JAVA_PARAM="${JAVA_PARAM} -XX:MetaspaceSize=256m"
	JAVA_PARAM="${JAVA_PARAM} -XX:CompressedClassSpaceSize=256m"
	JAVA_PARAM="${JAVA_PARAM} -XX:MinMetaspaceFreeRatio=50"
	JAVA_PARAM="${JAVA_PARAM} -XX:MaxMetaspaceFreeRatio=80"

	nohup java ${JAVA_PARAM} ${JAR} > nohup.out 2>&1 &

	action $"sucess, java process start $prog: " /bin/true
	return 0
}

stop()
{
	echo "stopping"
	exists
	if [ $? -ne 0 ]; then
		kill -15 ${PID}
	fi

	for (( i=0; i<300; i++)); do
		exists
		if [ $? -ne 0 ]; then
			sleep 1
		else
			break
		fi
	done

	exists
	if [ $? -ne 0 ]; then
		action $"failed $prog: " /bin/false
		return 1
	fi
	action $"sucess $prog: " /bin/true
	return 0
}

restart()
{
	stop
	if [ $? -eq 0 ]; then
		start
	fi
}

###########################################
if [ $# -ne 1 ]; then
	echo "$0 start|restart|stop"
	exit
fi

case "$1" in
"start") start
exit
;;
"restart") restart
exit
;;
"stop") stop
exit
;;
*) echo "$0 start|restart|stop"
exit
;;
esac
