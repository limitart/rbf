/*
 *
 *  * Copyright (c) 2016-present The Limitart Project
 *  *
 *  * Licensed under the Apache License, Version 2.0 (the "License");
 *  * you may not use this file except in compliance with the License.
 *  * You may obtain a copy of the License at
 *  *
 *  *   http://www.apache.org/licenses/LICENSE-2.0
 *  *
 *  * Unless required by applicable law or agreed to in writing, software
 *  * distributed under the License is distributed on an "AS IS" BASIS,
 *  * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  * See the License for the specific language governing permissions and
 *  * limitations under the License.
 *
 */

package top.limitart.rbf.account;

import io.netty.channel.EventLoop;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import top.limitart.net.NettySessionActor;
import top.limitart.net.Session;
import top.limitart.net.binary.BinaryMessage;
import top.limitart.rbf.lobby.Player;
import top.limitart.util.TimeUtil;

/**
 * 账户信息
 *
 * @author hank
 * @version 2018/9/4 0004 13:57
 */
public class Account extends NettySessionActor<BinaryMessage> {
    private static Logger LOGGER = LoggerFactory.getLogger(Account.class);
    private long uniqueID; // 账号id
    // 账号
    private String username;
    // 创建时间
    private long createTime = TimeUtil.now();
    // 玩家大厅角色
    private Player player;

    public Account(long uniqueID, String username, Player player) {
        this.uniqueID = uniqueID;
        this.username = username;
        this.player = player;
    }

    public void writeNow(BinaryMessage msg) {
        Session<BinaryMessage, EventLoop> where = where();
        if (where == null) {
            LOGGER.error("发送消息失败，找不到session，消息:{}", msg.getClass().getName());
            return;
        }
        where.writeNow(
                msg,
                (b, throwable) -> {
                    if (!b) {
                        if (throwable == null) {
                            LOGGER.error("发送消息失败,{}==>{}", where, msg.getClass().getName());
                        } else {
                            LOGGER.error("发送消息失败", throwable);
                        }
                    }
                });
    }

    public long getUniqueID() {
        return uniqueID;
    }

    public String getUsername() {
        return username;
    }

    public long getCreateTime() {
        return createTime;
    }

    public Player getPlayer() {
        return player;
    }

    @Override
    public String toString() {
        return "Account{" + "uniqueID=" + uniqueID + ", username='" + username + '\'' + '}';
    }

    @Override
    public void leave(Session<BinaryMessage, EventLoop> oldPlace) {
        super.leave(oldPlace);
        oldPlace.close();
        LOGGER.info("account {} leave res", this);
    }
}
