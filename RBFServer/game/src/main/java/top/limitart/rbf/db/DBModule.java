/*
 *
 *  * Copyright (c) 2016-present The Limitart Project
 *  *
 *  * Licensed under the Apache License, Version 2.0 (the "License");
 *  * you may not use this file except in compliance with the License.
 *  * You may obtain a copy of the License at
 *  *
 *  *   http://www.apache.org/licenses/LICENSE-2.0
 *  *
 *  * Unless required by applicable law or agreed to in writing, software
 *  * distributed under the License is distributed on an "AS IS" BASIS,
 *  * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  * See the License for the specific language governing permissions and
 *  * limitations under the License.
 *
 */

package top.limitart.rbf.db;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import top.limitart.base.Couple;
import top.limitart.base.label.LongRunning;
import top.limitart.collections.UnrepeatableQueue;
import top.limitart.concurrent.TaskQueue;
import top.limitart.db.sql.Prepare;
import top.limitart.db.sql.Update;
import top.limitart.db.sql.Where;
import top.limitart.json.JSON;
import top.limitart.json.JSONException;
import top.limitart.rbf.GameMode;
import top.limitart.rbf.Module;
import top.limitart.rbf.ServerConfig;
import top.limitart.db.DBDataSource;
import top.limitart.db.DBHandler;
import top.limitart.rbf.account.Account;
import top.limitart.rbf.account.AccountModule;
import top.limitart.singleton.Ref;
import top.limitart.singleton.Singleton;

import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.sql.*;
import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.Executor;
import java.util.concurrent.TimeUnit;

/**
 * 数据库
 *
 * @author hank
 * @version 2018/9/4 0004 17:55
 */
@Singleton
public class DBModule implements Module, Executor {
    private static Logger LOGGER = LoggerFactory.getLogger(DBModule.class);
    private static final String TABLE_SQL = "tables.sql";
    private TaskQueue dbThread = TaskQueue.create("db");
    private UnrepeatableQueue<Account> accountSaveQueue = new UnrepeatableQueue<>();
    @Ref
    ServerConfig serverConfig;
    @Ref
    AccountModule accountModule;
    DBHandler dbHandler;
    DBDataSource dataSource;

    @Override
    public void init() throws Exception {
        if (serverConfig.getGameMode() == GameMode.MEMORY) {
            dbHandler = DBHandler.createFake();
        } else {
            // 创建数据库
            String pureUrl =
                    serverConfig.getGameDBUrl().substring(0, serverConfig.getGameDBUrl().lastIndexOf("/"));
            String database =
                    serverConfig
                            .getGameDBUrl()
                            .split("[?]")[0]
                            .substring(serverConfig.getGameDBUrl().lastIndexOf("/") + 1);
            String sql =
                    "CREATE DATABASE IF NOT EXISTS `"
                            + database
                            + "` DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci;";
            try (DBDataSource tmp =
                         DBDataSource.createHikariCP(
                                 pureUrl, serverConfig.getDbUsr(), serverConfig.getDbPwd());
                 Connection tmpConnection = tmp.getConnection();
                 PreparedStatement statement = tmpConnection.prepareStatement(sql)) {
                if (statement.executeUpdate() > 0) {
                    LOGGER.info(sql);
                }
            }
            // 初始化数据库表
            dataSource =
                    DBDataSource.createHikariCP(
                            serverConfig.getGameDBUrl(), serverConfig.getDbUsr(), serverConfig.getDbPwd());
            dbHandler = DBHandler.createReal(dataSource);
            try (Connection connection = dataSource.getConnection()) {
                InputStream stream;
                File file = new File(TABLE_SQL);
                if (file.exists()) {
                    stream = new FileInputStream(file);
                } else {
                    stream = new FileInputStream(this.getClass().getResource("/" + TABLE_SQL).getFile());
                }
                StringBuilder sb = new StringBuilder();
                byte[] buf = new byte[1024];
                int len;
                while ((len = stream.read(buf)) > 0) {
                    sb.append(new String(buf, 0, len));
                }
                String sqls = sb.toString();
                stream.close();
                for (String s : sqls.split("[;]")) {
                    try (PreparedStatement tablePrepare = connection.prepareStatement(s)) {
                        tablePrepare.execute();
                        LOGGER.info(s);
                    }
                }
            }
        }
        //初始化玩家定时保存
        dbThread.schedule(() -> saveAccounts(), 1, TimeUnit.MINUTES);
    }

    /**
     * 异步保存玩家
     *
     * @param account
     */
    public void saveAccountAsync(Account account) {
        accountSaveQueue.offer(account);
    }

    /**
     * 定时保存玩家策略
     */
    private void saveAccounts() {
        //按策略取出保存
        //TODO 暂时每次取100个
        List<Account> toSaves = new LinkedList<>();
        accountSaveQueue.pollTo(100, a -> toSaves.add(a));
        updateAccounts(toSaves);
    }

    @Override
    public void destroy() {
        //所有玩家同步保存一次
        List<Account> toSaves = new LinkedList<>();
        accountModule.forEachAccount(a -> toSaves.add(a));
        LOGGER.info("开始保存所有玩家，玩家缓存大小:{}", toSaves.size());
        updateAccounts(toSaves);
        try {
            dataSource.close();
        } catch (Exception e) {
            LOGGER.error("datasource close error", e);
        }
    }

    /**
     * 查找帐号
     *
     * @param accountID
     */
    @LongRunning
    public Account selectAccount(long accountID) {
        try {
            return dbHandler.selectOne(
                    Prepare.select("data").from("account").where(Where.create().whereEquals("id", accountID)),
                    resultSet -> {
                        byte[] data = resultSet.getBytes("data");
                        try {
                            Account account = JSON.getDefault().toObj(data, Account.class);
                            return account;
                        } catch (JSONException e) {
                            LOGGER.error("解析Account数据错误", e);
                        }
                        return null;
                    });
        } catch (SQLException e) {
            LOGGER.error("从数据库加载Account错误", e);
        }
        return null;
    }

    @LongRunning
    public Account selectAccount(String username) {
        try {
            return dbHandler.selectOne(
                    Prepare.select("data")
                            .from("account")
                            .where(Where.create().whereEquals("username", username)),
                    resultSet -> {
                        byte[] data = resultSet.getBytes("data");
                        try {
                            Account account = JSON.getDefault().toObj(data, Account.class);
                            return account;
                        } catch (JSONException e) {
                            LOGGER.error("解析Account数据错误", e);
                        }
                        return null;
                    });
        } catch (SQLException e) {
            LOGGER.error("从数据库加载Account错误", e);
        }
        return null;
    }

    /**
     * 查找所有账号的用户名和编号
     *
     * @return
     */
    public List<Couple<String, Long>> selectUsernameAndIDs() {
        try {
            return dbHandler.selectList(Prepare.select("id", "username").from("account"), resultSet -> {
                long id = resultSet.getLong("id");
                String username = resultSet.getString("username");
                return Couple.ofImmutable(username, id);
            });
        } catch (SQLException e) {
            LOGGER.error("从数据库加载账号列表错误", e);
        }
        return null;
    }

    /**
     * 插入一个账号
     *
     * @param account
     * @return
     */
    public boolean insertAccount(Account account) {
        try {
            byte[] bytes = JSON.getDefault().toBytes(account);
            return dbHandler.update(Prepare.insert().into("account").values("id", account.getUniqueID(), "username", account.getUsername(), "create_time", account.getCreateTime(), "data", bytes)) > 0;
        } catch (JSONException | SQLException e) {
            LOGGER.error("插入帐号到数据库错误", e);
        }
        return false;
    }

    /**
     * 批量更新账号
     *
     * @param accounts
     * @return
     */
    private void updateAccounts(List<Account> accounts) {
        if (accounts == null || accounts.isEmpty()) {
            return;
        }
        List<Update> updates = new LinkedList<>();
        for (Account account : accounts) {
            try {
                byte[] bytes = JSON.getDefault().toBytes(account);
                Update where = Prepare.update("account").set("data", bytes).where(Where.create().whereEquals("id", account.getUniqueID()));
                updates.add(where);
            } catch (JSONException e) {
                LOGGER.error("序列化账号错误" + account.getUniqueID(), e);
            }
        }
        try {
            if (!updates.isEmpty()) {
                int i = dbHandler.updateBatchWithPrepare(updates);
                LOGGER.info("save account batch {}", i);
            }
        } catch (SQLException e) {
            LOGGER.error("更新帐号到数据库错误", e);
        }
    }

    @Override
    public void execute(Runnable command) {
        dbThread.execute(command);
    }

}
