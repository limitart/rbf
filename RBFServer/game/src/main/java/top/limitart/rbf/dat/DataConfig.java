package top.limitart.rbf.dat;

import io.netty.buffer.ByteBuf;
import io.netty.buffer.ByteBufAllocator;
import top.limitart.dat.DataContainer;
import top.limitart.dat.DataMeta;
import top.limitart.dat.DataSet;
import top.limitart.rbf.account.msg.AccountMessage;
import top.limitart.singleton.Singleton;
import top.limitart.util.CodecUtil;
import top.limitart.util.FileUtil;

import java.io.File;
import java.util.List;

/**
 * 静态数据集
 *
 * @author hank
 * @version 2018/10/17 0017 21:21
 */
@Singleton
public class DataConfig {
    private DataSet dataSet;
    private byte[] bytes;

    public void load() throws Exception {
        DataSet temp = new DataSet();
        List<File> dats = FileUtil.getFiles(new File("./dat"), "bytes");
        ByteBuf buf = ByteBufAllocator.DEFAULT.buffer();
        AccountMessage.DataSets sets = new AccountMessage.DataSets();
        for (File dat : dats) {
            String name = dat.getName().split("[.]")[0];
            byte[] bytes = FileUtil.readFile1(dat);
            Class clazz = Class.forName("top.limitart.rbf.dat." + name);
            temp.load(clazz, bytes, d -> d);
            AccountMessage.DataInfo dataInfo = new AccountMessage.DataInfo();
            dataInfo.dataName = name;
            dataInfo.dataBin = bytes;
            sets.datas.add(dataInfo);
        }
        sets.buffer(buf);
        sets.encode();
        byte[] buffer = new byte[buf.readableBytes()];
        buf.readBytes(buffer);
        buf.clear();
        bytes = CodecUtil.toGZip(buffer);
//        DataSet temp =
//                DataSet.withDir(
//                        new File("./dat"),
//                        "bytes",
//                        f -> "top.limitart.rbf.dat." + f.getName().split("[.]")[0],
//                        d -> d);
        dataSet = temp;
    }

    public byte[] getBytes() {
        return bytes;
    }

    public <T extends DataMeta> DataContainer<T> data(Class<T> beanClass) {
        return dataSet.getContainer(beanClass);
    }
}
