/*
 *
 *  * Copyright (c) 2016-present The Limitart Project
 *  *
 *  * Licensed under the Apache License, Version 2.0 (the "License");
 *  * you may not use this file except in compliance with the License.
 *  * You may obtain a copy of the License at
 *  *
 *  *   http://www.apache.org/licenses/LICENSE-2.0
 *  *
 *  * Unless required by applicable law or agreed to in writing, software
 *  * distributed under the License is distributed on an "AS IS" BASIS,
 *  * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  * See the License for the specific language governing permissions and
 *  * limitations under the License.
 *
 */

package top.limitart.rbf;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import top.limitart.base.Conditions;

import java.util.Collections;
import java.util.LinkedList;
import java.util.List;

/** 管理器 Created by Hank on 2018/9/3 */
public class Modules {
  private static Logger LOGGER = LoggerFactory.getLogger(Modules.class);
  private List<Class<? extends Module>> managersSeq = new LinkedList<>();

  public static Modules empty() {
    return new Modules();
  }

  public static Modules start(Class<? extends Module> managerClazz) {
    return empty().then(managerClazz);
  }

  private Modules() {}

  public Modules then(Class<? extends Module> managerClazz) {
    Conditions.args(!managersSeq.contains(managerClazz), "manager duplicated:%s", managerClazz);
    managersSeq.add(managerClazz);
    return this;
  }

  public void initAll() {
    managersSeq.forEach(
        c -> {
          try {
            GameBootstrap.SINGLETONS.instance(c).init();
          } catch (Exception e) {
            LOGGER.error("init module error", e);
            System.exit(1);
          }
          LOGGER.info("{} init done.", c.getClass().getName());
        });
    GameBootstrap.SINGLETONS.forEach(
        a -> {
          if (a instanceof Module && !managersSeq.contains(a)) {
            try {
              ((Module) a).init();
            } catch (Exception e) {
              LOGGER.error("init module error", e);
              System.exit(1);
            }
            LOGGER.info("{} init done.", a.getClass().getName());
          }
        });
  }

  public void destroyAll() {
    GameBootstrap.SINGLETONS.forEach(
        a -> {
          if (a instanceof Module && !managersSeq.contains(a)) {
            ((Module) a).destroy();
            LOGGER.info("{} destroy done.", a.getClass().getName());
          }
        });
    List<Class<? extends Module>> tmp = new LinkedList<>(managersSeq);
    Collections.reverse(tmp);
    tmp.forEach(
        c -> {
          GameBootstrap.SINGLETONS.instance(c).destroy();
          LOGGER.info("{} destroy done.", c.getClass().getName());
        });
  }
}
