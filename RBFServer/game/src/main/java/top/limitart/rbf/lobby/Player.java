package top.limitart.rbf.lobby;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import top.limitart.base.WeakRefHolder;
import top.limitart.concurrent.AbstractActor;
import top.limitart.concurrent.TaskQueue;
import top.limitart.net.binary.BinaryMessage;
import top.limitart.rbf.account.Account;
import top.limitart.rbf.account.msg.AccountMessage;
import top.limitart.rbf.battle.Fighter;

/**
 * 玩家 Created by Hank on 2018/11/2
 */
public class Player extends AbstractActor<TaskQueue, Fighter> {
    private static Logger LOGGER = LoggerFactory.getLogger(Player.class);
    //账户弱引用
    private transient WeakRefHolder<Account> accountHolder = WeakRefHolder.empty();
    //上次保存时间
    private transient long lastSaveTime;
    //玩家唯一ID
    private long uniqueID;
    //玩家昵称
    private String name;
    //玩家头像
    private int avatar;

    public void writeNow(BinaryMessage message) {
        Account account = getAccount();
        if (account == null) {
            LOGGER.info("玩家:{}发送消息失败，找不到账户引用", this);
            return;
        }
        account.writeNow(message);
    }

    public AccountMessage.MyPlayerInfo toMyPlayerInfo() {
        AccountMessage.MyPlayerInfo info = new AccountMessage.MyPlayerInfo();
        info.uniqueID = this.uniqueID;
        info.avatar = this.avatar;
        info.name = this.name;
        return info;
    }

    @Override
    public String toString() {
        return "Player{" +
                "uniqueID=" + uniqueID +
                ", name='" + name + '\'' +
                '}';
    }

    public long getLastSaveTime() {
        return lastSaveTime;
    }

    public void setLastSaveTime(long lastSaveTime) {
        this.lastSaveTime = lastSaveTime;
    }

    public void setAccount(Account account) {
        accountHolder.set(account);
    }

    public Account getAccount() {
        return accountHolder.get();
    }

    public long getUniqueID() {
        return uniqueID;
    }

    public void setUniqueID(long uniqueID) {
        this.uniqueID = uniqueID;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getAvatar() {
        return avatar;
    }

    public void setAvatar(int avatar) {
        this.avatar = avatar;
    }
}
