package top.limitart.rbf.battle;


import top.limitart.concurrent.Actor;
import top.limitart.concurrent.TaskQueue;


/**
 * 战场(占用线程的资源)
 * Created by Hank on 2018/11/2
 */
public abstract class BattleField implements Actor.Place<TaskQueue> {
    private TaskQueue taskQueue;

    public void setTaskQueue(TaskQueue taskQueue) {
        this.taskQueue = taskQueue;
    }

    @Override
    public TaskQueue res() {
        return taskQueue;
    }

    /**
     * 获取这个战场所有的战士
     *
     * @return
     */
    abstract Fighter[] allFighters();

    /**
     * 战场帧循环
     *
     * @param deltaTime
     */
    abstract void onLoop(long deltaTime);
}
