/*
 *
 *  * Copyright (c) 2016-present The Limitart Project
 *  *
 *  * Licensed under the Apache License, Version 2.0 (the "License");
 *  * you may not use this file except in compliance with the License.
 *  * You may obtain a copy of the License at
 *  *
 *  *   http://www.apache.org/licenses/LICENSE-2.0
 *  *
 *  * Unless required by applicable law or agreed to in writing, software
 *  * distributed under the License is distributed on an "AS IS" BASIS,
 *  * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  * See the License for the specific language governing permissions and
 *  * limitations under the License.
 *
 */

package top.limitart.rbf.net;

import io.netty.channel.EventLoop;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import top.limitart.mapping.Router;
import top.limitart.net.AddressPair;
import top.limitart.net.EndPoint;
import top.limitart.net.NettyEndPointType;
import top.limitart.net.Session;
import top.limitart.net.binary.BinaryEndPoint;
import top.limitart.net.binary.BinaryMessage;
import top.limitart.net.binary.BinaryRequestParam;
import top.limitart.rbf.GameBootstrap;
import top.limitart.rbf.ServerConfig;
import top.limitart.rbf.account.Account;
import top.limitart.rbf.account.AccountModule;
import top.limitart.rbf.account.msg.AccountMessage;
import top.limitart.rbf.dat.DataConfig;
import top.limitart.rbf.db.DBModule;
import top.limitart.rbf.lobby.LobbyModule;
import top.limitart.singleton.Ref;
import top.limitart.singleton.Singleton;

/**
 * 游戏大厅服务器(也相当于网关)
 *
 * @author hank
 * @version 2018/9/4 0004 16:23
 */
@Singleton
public class LobbyServer {
    private static Logger LOGGER = LoggerFactory.getLogger(LobbyServer.class);
    @Ref
    ServerConfig serverConfig;
    @Ref
    AccountModule accountModule;
    @Ref
    LobbyModule lobbyModule;
    @Ref
    DataConfig dataConfig;
    private EndPoint server;
    // 链接战场服务器的端点，这里暂时就一个战场服务器，
    private EndPoint battleClient;

    public void startServer() throws Exception {
        server =
                BinaryEndPoint.server()
                        .name("lobby-server")
                        .router(
                                Router.create(
                                        BinaryMessage.class,
                                        RBFRequest.class,
                                        "",
                                        c -> GameBootstrap.SINGLETONS.instance(c)))
                        .onExceptionThrown(
                                (s, t) -> {
                                    s.close();
                                    LOGGER.error("lobby server error", t);
                                }).onConnected((s, b) -> {
                    //断开链接
                    if (!b) {
                        // 处理退出流程
                        long accountID = s.params().getLong(AccountModule.SESSION_PARAM_ACCOUNT_ID);
                        Account account = accountModule.getAccount(accountID);
                        if (account != null) {
                            Session<BinaryMessage, EventLoop> where = account.where();
                            if (where != null && where == s) {
                                //如果这个账号是这个链接的话，则代表不是被顶号而是真正退出了
                                lobbyModule.execute(() -> {
                                    lobbyModule.onQuitLobby(account);
                                    //关闭资源
                                    where.res().execute(() -> account.leave(s));
                                });
                            }
                        }
                    }
                    //链接上，发送静态数据文件
                    else {
                        byte[] bytes = dataConfig.getBytes();
                        AccountMessage.ResDataSet res = new AccountMessage.ResDataSet();
                        res.data = bytes;
                        s.writeNow(res);
                        LOGGER.info("向{}发送静态资源，大小:{}", s, bytes.length);
                    }
                })
                        .onMessageIn(
                                (s, m, f) -> {
                                    // TODO 查看f能不能处理，如果不能处理，就转发给其他的服务器处理
                                    // 当有消息进来如何处理
                                    // TODO 暂时这么处理
                                    long accountID = s.params().getLong(AccountModule.SESSION_PARAM_ACCOUNT_ID);
                                    Account account = accountModule.getAccount(accountID);
                                    if (account == null || account.getPlayer() == null) {
                                        f.request(m, () -> new RBFRequest(s, null, m), i -> i.invoke());
                                    } else {
                                        lobbyModule.execute(() -> f.request(m, () -> new RBFRequest(s, account.getPlayer(), m), i -> i.invoke()));
                                    }
                                })
                        .build()
                        .start(new AddressPair(serverConfig.getGamePort()));
        battleClient =
                BinaryEndPoint.builder(NettyEndPointType.CLIENT_LOCAL)
                        .timeoutSeconds(0)
                        .name("lobby-battle-local")
                        .onExceptionThrown((s, t) -> LOGGER.error("battle client error", t))
                        .autoReconnect(1)
                        .router(Router.empty(BinaryMessage.class, BinaryRequestParam.class))
                        .onMessageIn((s, m, f) -> {
                        })
                        .build()
                        .start(new AddressPair(0));
    }

    public void stopServer() throws Exception {
        server.stop();
    }

    /**
     * 找到对象当前的战斗服务器
     *
     * @param o
     * @return
     */
    public EndPoint findBattleClient(Object o) {
        // 这里暂时就实现单连，理想是有一个public服务器，告知所有大厅服有哪些战斗服，一场战斗去分配到哪个战斗服执行
        return this.battleClient;
    }
}
