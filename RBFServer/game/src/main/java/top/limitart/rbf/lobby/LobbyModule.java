package top.limitart.rbf.lobby;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import top.limitart.concurrent.TaskQueue;
import top.limitart.rbf.Module;
import top.limitart.rbf.account.Account;
import top.limitart.rbf.db.DBModule;
import top.limitart.singleton.Ref;
import top.limitart.singleton.Singleton;
import top.limitart.util.TimeUtil;

import java.util.concurrent.Executor;
import java.util.concurrent.TimeUnit;

/**
 * 大厅
 *
 * @author hank
 * @version 2018/11/6 0006 21:13
 */
@Singleton
public class LobbyModule implements Module, Executor {
    private static Logger LOGGER = LoggerFactory.getLogger(LobbyModule.class);
    private TaskQueue lobbyThread = TaskQueue.create("lobby");
    @Ref
    DBModule dbModule;

    @Override
    public void init() throws Exception {
    }

    @Override
    public void destroy() {
    }

    @Override
    public void execute(Runnable command) {
        lobbyThread.execute(command);
    }

    /**
     * 当登录到大厅
     *
     * @param account
     */
    public void onLoginLobby(Account account) {
        account.where().res().schedule(() -> execute(() -> onPlayerHeatbeat(account)), 1, TimeUnit.SECONDS);
    }

    /**
     * 玩家心跳
     *
     * @param account
     */
    private void onPlayerHeatbeat(Account account) {
        long now = TimeUtil.now();
        if (now - account.getPlayer().getLastSaveTime() > TimeUnit.MINUTES.toMillis(1)) {
            dbModule.saveAccountAsync(account);
            account.getPlayer().setLastSaveTime(now);
        }
    }

    public void onQuitLobby(Account account) {
        //保存账号
        dbModule.saveAccountAsync(account);
    }
}
