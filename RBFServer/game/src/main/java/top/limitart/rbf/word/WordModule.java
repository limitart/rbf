/*
 *
 *  * Copyright (c) 2016-present The Limitart Project
 *  *
 *  * Licensed under the Apache License, Version 2.0 (the "License");
 *  * you may not use this file except in compliance with the License.
 *  * You may obtain a copy of the License at
 *  *
 *  *   http://www.apache.org/licenses/LICENSE-2.0
 *  *
 *  * Unless required by applicable law or agreed to in writing, software
 *  * distributed under the License is distributed on an "AS IS" BASIS,
 *  * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  * See the License for the specific language governing permissions and
 *  * limitations under the License.
 *
 */

package top.limitart.rbf.word;

import top.limitart.rbf.Module;
import top.limitart.singleton.Singleton;
import top.limitart.util.SensitiveWords;
import top.limitart.util.StringUtil;

/**
 * 名字管理
 *
 * @author hank
 * @version 2018/9/4 0004 17:04
 */
@Singleton
public class WordModule implements Module {
  private SensitiveWords sensitiveWords = new SensitiveWords();

  @Override
  public void init() {
    //    sensitiveWords.init();
  }

  @Override
  public void destroy() {}

  public boolean legal(String content) {
    // 要求为汉字,英文,数字,以及允许的特殊字符
    for (char c : content.toCharArray()) {
      if (!StringUtil.isChinese(c) && !Character.isDigit(c) && !Character.isLetter(c)) {
        return false;
      }
    }
    if (hasSensitiveWord(content)) {
      return false;
    }
    if (StringUtil.isSQLRelative(content)) {
      return false;
    }
    return true;
  }

  public boolean hasSensitiveWord(String content) {
    return sensitiveWords.hasBadWords(content);
  }
}
