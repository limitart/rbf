/*
 *
 *  * Copyright (c) 2016-present The Limitart Project
 *  *
 *  * Licensed under the Apache License, Version 2.0 (the "License");
 *  * you may not use this file except in compliance with the License.
 *  * You may obtain a copy of the License at
 *  *
 *  *   http://www.apache.org/licenses/LICENSE-2.0
 *  *
 *  * Unless required by applicable law or agreed to in writing, software
 *  * distributed under the License is distributed on an "AS IS" BASIS,
 *  * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  * See the License for the specific language governing permissions and
 *  * limitations under the License.
 *
 */

package top.limitart.rbf.id;

import top.limitart.rbf.Module;
import top.limitart.game.UniqueID;
import top.limitart.singleton.Singleton;

/**
 * ID生成器
 *
 * @author hank
 * @version 2018/9/4 0004 17:16
 */
@Singleton
public class IDModule implements Module {
  private UniqueID.Seed accountSeed = UniqueID.Seed.create();

  @Override
  public void init() {}

  @Override
  public void destroy() {}

  /**
   * 获取一个唯一的账号ID
   *
   * @return
   */
  public long nextAccountID() {
    return UniqueID.nextID(1, accountSeed);
  }
}
