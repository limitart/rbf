package top.limitart.rbf.battle;


import top.limitart.concurrent.Actor;
import top.limitart.concurrent.TaskQueue;

/**
 * 战场上的战士
 * Created by Hank on 2018/11/2
 */
public interface Fighter extends Actor<TaskQueue, BattleField> , Actor.Place<TaskQueue> {
}
