package top.limitart.rbf.battle;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import top.limitart.base.Conditions;
import top.limitart.base.LimitartRuntimeException;
import top.limitart.concurrent.DisruptorTaskQueue;
import top.limitart.concurrent.NamedThreadFactory;
import top.limitart.concurrent.TaskQueue;
import top.limitart.concurrent.TaskQueueGroup;
import top.limitart.game.UniqueID;
import top.limitart.rbf.Module;
import top.limitart.singleton.Singleton;
import top.limitart.util.TimeUtil;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

/** 战场管理调控器 Created by Hank on 2018/11/2 */
@Singleton
public class BattleFields implements Module {
  private static Logger LOGGER = LoggerFactory.getLogger(BattleFields.class);
  // 服务器战场帧率
  private static final int FPS = 60;
  // 战场编号生成器
  private UniqueID.Seed idCreator = UniqueID.Seed.create();
  // 战场线程租
  private TaskQueueGroup threadGroup =
      new TaskQueueGroup("battle-field-group", 5, s -> DisruptorTaskQueue.create(s));
  // 战场实体组
  private Map<Long, BattleField> battles = new ConcurrentHashMap<>();
  // 战场帧循环
  private ScheduledExecutorService fpsExecutor =
      Executors.newSingleThreadScheduledExecutor(
          new NamedThreadFactory() {
            @Override
            public String namePrefix() {
              return "battle-main-loop";
            }
          });
  // 上一次循环的时间
  private long lastLoopTime = TimeUtil.now();

  public static void main(String[] args) throws Exception {
    Fighter fighter = new AbstractFighter() {};
    BattleField battleField = new AbstractBattleField(new Fighter[] {fighter}) {};

    BattleFields battleFields = new BattleFields();
    battleFields.init();
    battleFields.boostrapABattle(battleField);
  }

  /**
   * 启动一个战场，让他自动化运行起来
   *
   * @param battleField
   */
  public void boostrapABattle(BattleField battleField) {
    LOGGER.info("开始加载战场:{}", battleField);
    Conditions.notNull(battleField);
    // 给此战场分配一个处理线程
    TaskQueue thread = threadGroup.next();
    // 让战场Actor去占据这个线程资源
    battleField.setTaskQueue(thread);
    LOGGER.info("分配战场:{},到线程:{}", battleField, thread.thread().getName());
    Conditions.args(battleField.allFighters() != null && battleField.allFighters().length > 0);
    for (Fighter fighter : battleField.allFighters()) {
      fighter.joinWhenFree(
          battleField,
          () -> {
            LOGGER.info("战士:{},分配到战场:{},成功", fighter, battleField);
          },
          e -> {
            throw new LimitartRuntimeException("战士:%s,分配到战场:%s,成功", fighter, battleField);
          });
    }
    // 给战场分配ID
    long battleFiledID = UniqueID.nextID(1, idCreator);
    Conditions.args(!battles.containsKey(battleFiledID), "战场ID重复？");
    battles.put(battleFiledID, battleField);
    LOGGER.info("加载战场:{},成功,分配ID为:{}", battleField, battleFiledID);
  }

  @Override
  public void init() throws Exception {
    // 启动战场帧循环
    fpsExecutor.scheduleAtFixedRate(
        () -> {
          long now = TimeUtil.now();
          long deltaTime = now - lastLoopTime;
          lastLoopTime = now;
          // 这里暂时就这么循环
          for (BattleField value : battles.values()) {
            // 获取处理线程资源
            TaskQueue res = value.res();
            if (res == null) {
              LOGGER.error("战场:{}没有初始化完毕！！！", value);
              continue;
            }
            res.execute(() -> value.onLoop(deltaTime));
          }
        },
        0,
        FPS,
        TimeUnit.MILLISECONDS);
  }

  @Override
  public void destroy() {}
}
