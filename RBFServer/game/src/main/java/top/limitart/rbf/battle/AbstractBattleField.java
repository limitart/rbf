package top.limitart.rbf.battle;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import top.limitart.base.Conditions;
import top.limitart.concurrent.TaskQueue;
import top.limitart.fsm.Event;
import top.limitart.fsm.FSM;
import top.limitart.fsm.State;
import top.limitart.fsm.StateException;

import java.util.concurrent.TimeUnit;

/**
 * 抽象战场层
 * Created by Hank on 2018/11/2
 */
public abstract class AbstractBattleField extends BattleField {
    private static Logger LOGGER = LoggerFactory.getLogger(AbstractBattleField.class);
    //=======================================一堆状态
    private static final int WAIT_ALL_FIGHTER_CONNECT_STATE = 1;
    //等待所有玩家连接最大等待时间
    private static final long MAX_WAIT_SECOND = TimeUnit.SECONDS.toMillis(10);
    //等待所有玩家连进游戏的状态
    private State waitAllFighterConnectState = new WaitAllFighterConnectState();
    //=======================================一堆状态
    //状态机
    private FSM fsm = new FSM();
    //所有玩家集合
    private Fighter[] fighters;

    public AbstractBattleField(Fighter[] fighters) throws StateException {
        Conditions.args(fighters != null && fighters.length > 0, "must have fighters");
        this.fighters = fighters;
        fsm.addState(waitAllFighterConnectState);
        fsm.start();
    }

    @Override
    Fighter[] allFighters() {
        return fighters;
    }


    @Override
    void onLoop(long deltaTime) {
        try {
            fsm.loop();
        } catch (StateException e) {
            LOGGER.error("帧循环错误", e);
        }
    }

    private class WaitAllFighterConnectState extends State<FSM> {
        private long waitTime = 0;

        @Override
        public Integer getStateId() {
            return WAIT_ALL_FIGHTER_CONNECT_STATE;
        }

        @Override
        public void onEnter(State<FSM> state, FSM fsm) {
            LOGGER.info("战场:{},开始等待所有玩家连接进入...", AbstractBattleField.this);
        }

        @Override
        public void onExit(State<FSM> state, FSM fsm) {

        }

        @Override
        protected void onExecute(long l, FSM fsm) {
            waitTime += l;
            if (waitTime > MAX_WAIT_SECOND) {
                LOGGER.info("战场:{},超过最大等待时间:{},强制开始游戏", AbstractBattleField.this, MAX_WAIT_SECOND);
                finish();
            }
        }

        private class FinishEvent implements Event<FSM> {
            @Override
            public Integer getNextStateId() {
                return null;
            }

            @Override
            public boolean onCondition(FSM fsm, State<FSM> state, long l) {
                return false;
            }
        }
    }
}
