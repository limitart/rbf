/*
 *
 *  * Copyright (c) 2016-present The Limitart Project
 *  *
 *  * Licensed under the Apache License, Version 2.0 (the "License");
 *  * you may not use this file except in compliance with the License.
 *  * You may obtain a copy of the License at
 *  *
 *  *   http://www.apache.org/licenses/LICENSE-2.0
 *  *
 *  * Unless required by applicable law or agreed to in writing, software
 *  * distributed under the License is distributed on an "AS IS" BASIS,
 *  * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  * See the License for the specific language governing permissions and
 *  * limitations under the License.
 *
 */

package top.limitart.rbf;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import top.limitart.json.JSONException;
import top.limitart.singleton.Singleton;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

/**
 * 游戏配置
 *
 * @author hank
 * @version 2018/9/3 0003 14:34
 */
@Singleton
public class ServerConfig {
  private static Logger LOGGER = LoggerFactory.getLogger(ServerConfig.class);
  private static final String SERVER_CONFIG_FILE = "server-config.properties";
  // 游戏端口
  private int gamePort;
  // 后台端口
  private int backendPort;
  // 资源文件路径
  private String resUrl;
  private String gameDBUrl;
  private String dbUsr;
  private String dbPwd;
  // 启动模式
  private GameMode gameMode;

  public int getGamePort() {
    return gamePort;
  }

  public void setGamePort(int gamePort) {
    this.gamePort = gamePort;
  }

  public int getBackendPort() {
    return backendPort;
  }

  public void setBackendPort(int backendPort) {
    this.backendPort = backendPort;
  }

  public String getResUrl() {
    return resUrl;
  }

  public void setResUrl(String resUrl) {
    this.resUrl = resUrl;
  }

  public String getGameDBUrl() {
    return gameDBUrl;
  }

  public void setGameDBUrl(String gameDBUrl) {
    this.gameDBUrl = gameDBUrl;
  }

  public String getDbUsr() {
    return dbUsr;
  }

  public void setDbUsr(String dbUsr) {
    this.dbUsr = dbUsr;
  }

  public String getDbPwd() {
    return dbPwd;
  }

  public void setDbPwd(String dbPwd) {
    this.dbPwd = dbPwd;
  }

  public GameMode getGameMode() {
    return gameMode;
  }

  public void setGameMode(GameMode gameMode) {
    this.gameMode = gameMode;
  }

  public void init() throws IOException, JSONException {
    Properties properties = new Properties();
    InputStream stream;
    File file = new File(SERVER_CONFIG_FILE);
    if (file.exists()) {
      stream = new FileInputStream(file);
    } else {
      stream = new FileInputStream(this.getClass().getResource("/" + SERVER_CONFIG_FILE).getFile());
    }
    properties.load(stream);
    stream.close();
    setGamePort(Integer.parseInt(properties.getProperty("game_port")));
    setBackendPort(Integer.parseInt(properties.getProperty("backend_port")));
    setResUrl(properties.getProperty("res_url"));
    setGameDBUrl(properties.getProperty("db_url"));
    setDbUsr(properties.getProperty("db_usr"));
    setDbPwd(properties.getProperty("db_pwd"));
    // 设置系统变量
    this.gameMode = GameMode.valueOf(properties.getProperty("mode"));
    LOGGER.info("game start mode:========{}========", this.gameMode);
  }
}
