package top.limitart.rbf.net;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import top.limitart.mapping.Router;
import top.limitart.net.AddressPair;
import top.limitart.net.EndPoint;
import top.limitart.net.NettyEndPointType;
import top.limitart.net.binary.BinaryEndPoint;
import top.limitart.net.binary.BinaryMessage;
import top.limitart.rbf.GameBootstrap;
import top.limitart.singleton.Singleton;

/**
 * 战场服务器
 *
 * @author hank
 * @version 2018/10/17 0017 17:07
 */
@Singleton
public class BattleServer {
  private static Logger LOGGER = LoggerFactory.getLogger(LobbyServer.class);
  private EndPoint server;

  public void startServer() throws Exception {
    server =
        BinaryEndPoint.builder(NettyEndPointType.SERVER_LOCAL)
            .name("battle-server-local")
            .timeoutSeconds(0)
            .router(
                Router.create(
                    BinaryMessage.class,
                    RBFRequest.class,
                    "",
                    c -> GameBootstrap.SINGLETONS.instance(c)))
            .onExceptionThrown(
                (s, t) -> {
                  s.close();
                  LOGGER.error("lobby server error", t);
                })
            .onMessageIn(
                (s, m, f) -> {
                  // TODO 查看f能不能处理，如果不能处理，就转发给其他的服务器处理
                  // 当有消息进来如何处理
                  // TODO 暂时这么处理
                  f.request(m, () -> new RBFRequest(s, null, m), i -> i.invoke());
                })
            .build()
            .start(AddressPair.withPort(0));
  }

  public void stopServer() throws Exception {
    server.stop();
  }
}
