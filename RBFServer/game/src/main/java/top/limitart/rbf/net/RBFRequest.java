package top.limitart.rbf.net;

import top.limitart.net.Session;
import top.limitart.net.binary.BinaryMessage;
import top.limitart.net.binary.BinaryRequestParam;
import top.limitart.rbf.lobby.Player;

/**
 * 大厅请求-如果是此请求转发到大厅服务器上
 *
 * @author hank
 * @version 2018/10/17 0017 15:59
 */
public class RBFRequest extends BinaryRequestParam {
    private Player player;

    public RBFRequest(Session session, Player player, BinaryMessage msg) {
        super(session, msg);
        this.player = player;
    }

        public Player getPlayer() {
        return player;
    }
}
