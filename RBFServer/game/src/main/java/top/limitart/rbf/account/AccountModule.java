/*
 *
 *  * Copyright (c) 2016-present The Limitart Project
 *  *
 *  * Licensed under the Apache License, Version 2.0 (the "License");
 *  * you may not use this file except in compliance with the License.
 *  * You may obtain a copy of the License at
 *  *
 *  *   http://www.apache.org/licenses/LICENSE-2.0
 *  *
 *  * Unless required by applicable law or agreed to in writing, software
 *  * distributed under the License is distributed on an "AS IS" BASIS,
 *  * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  * See the License for the specific language governing permissions and
 *  * limitations under the License.
 *
 */

package top.limitart.rbf.account;

import io.netty.channel.EventLoop;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import top.limitart.base.Conditions;
import top.limitart.base.Couple;
import top.limitart.base.label.LongRunning;
import top.limitart.base.label.ThreadUnsafe;
import top.limitart.collections.ConcurrentLRUCache;
import top.limitart.concurrent.TaskQueue;
import top.limitart.mapping.Mapper;
import top.limitart.mapping.MapperClass;
import top.limitart.net.Session;
import top.limitart.net.binary.BinaryMessage;
import top.limitart.rbf.Module;
import top.limitart.rbf.account.msg.AccountMessage;
import top.limitart.rbf.db.DBModule;
import top.limitart.rbf.id.IDModule;
import top.limitart.rbf.lobby.LobbyModule;
import top.limitart.rbf.lobby.Player;
import top.limitart.rbf.net.RBFRequest;
import top.limitart.rbf.word.WordModule;
import top.limitart.singleton.Ref;
import top.limitart.singleton.Singleton;
import top.limitart.util.StringUtil;

import java.util.*;
import java.util.concurrent.Executor;
import java.util.concurrent.TimeUnit;
import java.util.function.Consumer;

/**
 * 帐号管理器 Created by Hank on 2018/9/4
 */
@Singleton
@MapperClass
public class AccountModule implements Module, Executor {
    private static Logger LOGGER = LoggerFactory.getLogger(AccountModule.class);
    private static long CACHE_ALIVE_TIME = TimeUnit.MINUTES.toMillis(5);
    public static int SESSION_PARAM_ACCOUNT_ID = 1;
    private static int MAX_CACHE_SIZE = 1000;
    // 帐号缓存(帐号数量大于1000才开始移除不在线的账号)
    private ConcurrentLRUCache<Long, AccountCacheable> cache = new ConcurrentLRUCache<>(MAX_CACHE_SIZE, true);
    // 用户名ID缓存
    private Map<String, Long> username2ID = new HashMap<>();
    // 玩家名集合
    private Set<String> names = new HashSet<>();
    // 登录线程
    private TaskQueue loginThread = TaskQueue.create("login");
    @Ref
    WordModule wordModule;
    @Ref
    IDModule idModule;
    @Ref
    DBModule dbModule;
    @Ref
    LobbyModule lobbyModule;

    @Override
    public void init() {
        // 从数据库加载账号列表
        List<Couple<String, Long>> couples = dbModule.selectUsernameAndIDs();
        if (couples != null) {
            LOGGER.info("加载账号列表{}个", couples.size());
            for (Couple<String, Long> couple : couples) {
                String username = couple.get1();
                long id = couple.get2();
                Conditions.args(!username2ID.containsKey(username), "帐号:%s重复", username);
                username2ID.put(username, id);
            }
        }
        //初始化所有名字
    }

    @Override
    public void destroy() {
    }

    /**
     * 客户端请求登录
     *
     * @param param
     */
    @Mapper(AccountMessage.ReqLoginInner.class)
    public void reqLoginInner(RBFRequest param) {
        AccountMessage.ReqLoginInner msg = param.msg();
        execute(() -> login(param.session(), msg.username));
    }

    public void login(Session session, String username) {
        Conditions.sameThread(loginThread.thread(), "登录操作必须在登录线程进行");
        if (StringUtil.empty(username) || username.length() > 32) {
            session.writeNow(AccountMessage.ResLoginFail.USERNAME_EMPTY_OR_GREATER_THAN_32);
            LOGGER.error("账号非法:{},长度超标", username);
            return;
        }
        // 这里判断去除中文，不然数据库存储那儿长度判断32跟这里的32不是一个意思
        if (StringUtil.hasChinese(username)) {
            session.writeNow(AccountMessage.ResLoginFail.NOT_CHINESE);
            LOGGER.error("账号非法:{},不能有中文", username);
            return;
        }
        // 查找帐号
        Account account = null;
        Long accountID = username2ID.get(username);
        if (accountID != null) {
            account = loadAccount(accountID);
        }
        if (account == null) {
            if (!wordModule.legal(username)) {
                session.writeNow(AccountMessage.ResLoginFail.USERNAME_ILEAGLE);
                LOGGER.error("账号非法:{}", username);
                return;
            }
            LOGGER.info("user:{}开始创建新的帐号...", username);
            long uid = idModule.nextAccountID();
            // 创建帐号
            account = new Account(uid, username, null);
            Conditions.args(!username2ID.containsKey(username), "帐号%s重复，插入失败", username);
            Conditions.args(!cache.containsKey(uid), "帐号ID%s重复,插入失败", uid);
            // 这里先同步插入
            Conditions.args(dbModule.insertAccount(account), "帐号%s,%s插入数据库失败", username, uid);
            username2ID.put(username, uid);
            cache.put(uid, new AccountCacheable(account));
            LOGGER.info("创建帐号:{}成功，当前缓存容量:{}", account, cache.size());
        }
        Account finalAccount = account;
        // 判断这个账号是否已经由网络绑定了，如果绑定了，则把那娃提下线
        Session<BinaryMessage, EventLoop> oldSession = account.where();
        if (oldSession != null) {
            if (oldSession == session) {
                // 自己登陆两次？
                session.writeNow(AccountMessage.ResLoginFail.LOGIN_AGAIN);
                return;
            } else {
                // 踢下线
                session.writeNow(AccountMessage.ResLoginFail.BE_LOGINED);
            }
        }
        finalAccount.forceJoin(
                session,
                () -> {
                    session.params().put(SESSION_PARAM_ACCOUNT_ID, finalAccount.getUniqueID());
                    onLoginSuccess(finalAccount);
                },
                (e) -> session.writeNow(AccountMessage.ResLoginFail.LOGIN_FAILED));
    }

    private void onLoginSuccess(Account account) {
        //  这里判断账号有没有创建Player，没创建就通知客户端创建，创建了就发送角色信息
        if (account.getPlayer() != null) {
            account.getPlayer().setAccount(account);
            AccountMessage.MyPlayerInfo myPlayerInfo = account.getPlayer().toMyPlayerInfo();
            AccountMessage.ResMyPlayerInfo msg = new AccountMessage.ResMyPlayerInfo();
            msg.myPlayerInfo = myPlayerInfo;
            account.writeNow(msg);
            lobbyModule.execute(() -> lobbyModule.onLoginLobby(account));
        } else {
            account.writeNow(new AccountMessage.ResMyPlayerInfo());
        }
    }

    @Mapper(AccountMessage.ReqCreateMyPlayer.class)
    public void reqCreatePlayer(RBFRequest param) {
        execute(() -> {
            Session<BinaryMessage, EventLoop> session = param.session();
            AccountMessage.ReqCreateMyPlayer msg = param.msg();
            long accountID = session.params().getLong(SESSION_PARAM_ACCOUNT_ID);
            if (accountID == 0) {
                session.writeNow(AccountMessage.ResCreateMyPlayerFail.NO_ACCOUNT);
                return;
            }
            Account account = getAccount(accountID);
            if (account == null) {
                session.writeNow(AccountMessage.ResCreateMyPlayerFail.NO_ACCOUNT);
                return;
            }
            Session<BinaryMessage, EventLoop> where = account.where();
            if (where == null) {
                session.writeNow(AccountMessage.ResCreateMyPlayerFail.ACCOUNT_NOT_LOGIN);
                return;
            }
            if (where != session) {
                session.writeNow(AccountMessage.ResCreateMyPlayerFail.ACCOUNT_NOT_BELONG_TO_YOU);
                return;
            }
            if (account.getPlayer() != null) {
                session.writeNow(AccountMessage.ResCreateMyPlayerFail.ALREADY_CREATED);
                return;
            }
            String name = msg.name;
            if (StringUtil.empty(name) || name.length() > 16 || name.contains(" ")) {
                session.writeNow(AccountMessage.ResCreateMyPlayerFail.NAME_TOO_LONG);
                return;
            }
            name = name.trim();
            if (names.contains(name)) {
                session.writeNow(AccountMessage.ResCreateMyPlayerFail.NAME_TOO_LONG);
                return;
            }
            if (!wordModule.legal(name)) {
                session.writeNow(AccountMessage.ResCreateMyPlayerFail.NAME_ILEAGLE);
                return;
            }
            //TODO 检查头像
            names.add(name);
            Player player = new Player();
            player.setUniqueID(accountID);
            player.setAvatar(msg.avatar);
            player.setName(name);
            player.setAccount(account);
            AccountMessage.ResCreateMyPlayerSuccess res = new AccountMessage.ResCreateMyPlayerSuccess();
            res.myPlayerInfo = player.toMyPlayerInfo();
            player.writeNow(res);
            lobbyModule.execute(() -> lobbyModule.onLoginLobby(account));
        });
    }

    /**
     * 从缓存中调取Account，如果没有则从数据库中调取
     *
     * @param accountID
     * @return
     */
    @ThreadUnsafe
    @LongRunning
    public Account loadAccount(long accountID) {
        Account account = getAccount(accountID);
        if (account == null) {
            account = dbModule.selectAccount(accountID);
            LOGGER.info("load account from database,where id = {}", account);
        }
        return account;
    }

    public Account getAccount(long accountID) {
        AccountCacheable accountCacheable = cache.get(accountID);
        if (accountCacheable == null) {
            return null;
        }
        return accountCacheable.getAccount();
    }

    public void forEachAccount(Consumer<Account> consumer) {
        cache.forEach((k, v) -> consumer.accept(v.getAccount()));
    }

    @Override
    public void execute(Runnable command) {
        loginThread.execute(command);
    }

    private static class AccountCacheable extends ConcurrentLRUCache.LRUCacheable {
        private final Account account;

        public AccountCacheable(Account account) {
            this.account = Conditions.notNull(account);
        }

        public Account getAccount() {
            return account;
        }

        @Override
        protected long aliveTime() {
            return CACHE_ALIVE_TIME;
        }

        @Override
        protected boolean removable0() {
            //断线的账号可清除
            return account.where() == null;
        }

        @Override
        public void whenRemove() {
            LOGGER.info("LRU remove account {}", account);
        }
    }
}
