/*
 *
 *  * Copyright (c) 2016-present The Limitart Project
 *  *
 *  * Licensed under the Apache License, Version 2.0 (the "License");
 *  * you may not use this file except in compliance with the License.
 *  * You may obtain a copy of the License at
 *  *
 *  *   http://www.apache.org/licenses/LICENSE-2.0
 *  *
 *  * Unless required by applicable law or agreed to in writing, software
 *  * distributed under the License is distributed on an "AS IS" BASIS,
 *  * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  * See the License for the specific language governing permissions and
 *  * limitations under the License.
 *
 */

package top.limitart.rbf;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import top.limitart.base.Conditions;
import top.limitart.json.JSONException;
import top.limitart.rbf.dat.DataConfig;
import top.limitart.rbf.db.DBModule;
import top.limitart.rbf.net.BattleServer;
import top.limitart.rbf.net.LobbyServer;
import top.limitart.singleton.Singletons;
import top.limitart.util.ReflectionUtil;

import java.io.IOException;
import java.util.List;

/**
 * 游戏启动入口
 *
 * @author hank
 * @version 2018/9/3 0003 14:34
 */
public class GameBootstrap {
    private static Logger LOGGER = LoggerFactory.getLogger(GameBootstrap.class);
    public static Singletons SINGLETONS = Singletons.create();
    public static Modules MODULES = Modules.empty();

    public static void main(String[] args) {
        LOGGER.info("=====Royal Battle Server Start=====");
        // 开始加载所有单例,这个服务器就不考虑热更了,取当前classloader就行
        SINGLETONS
                .search(GameBootstrap.class)
                .forEach(a -> LOGGER.info("load singleton:{}", a.getClass().getName()));
        // 初始化服务器配置
        try {
            LOGGER.info("load server config");
            SINGLETONS.instance(ServerConfig.class).init();
        } catch (IOException | JSONException e) {
            LOGGER.error("服务器配置错误", e);
            System.exit(1);
        }
        // 先加载静态数据
        try {
            LOGGER.info("load data config");
            SINGLETONS.instance(DataConfig.class).load();
        } catch (Exception e) {
            LOGGER.error("静态数据加载错误", e);
            System.exit(1);
        }

        // 初始化所有Manager
        MODULES.then(DBModule.class);
        try {
            MODULES.initAll();
        } catch (Exception e) {
            LOGGER.error("模块初始化错误", e);
            System.exit(1);
        }

        // 最后启动服务器端口
        // 先启动战斗服务器
        try {
            SINGLETONS.instance(BattleServer.class).startServer();
        } catch (Exception e) {
            LOGGER.error("battle server start error", e);
        }
        // 大厅逻辑服
        try {
            SINGLETONS.instance(LobbyServer.class).startServer();
        } catch (Exception e) {
            LOGGER.error("lobby server start error", e);
        }
        // TODO 游戏后台

        // 注册进程关闭钩子
        Runtime.getRuntime().addShutdownHook(new Thread(() -> onShutdown(), "shutdown-hook"));
    }

    private static void onShutdown() {
        //TODO 等待战斗打完

        // 先停战斗服
        try {
            SINGLETONS.instance(BattleServer.class).stopServer();
        } catch (Exception e) {
            LOGGER.error("battle server shutdown error", e);
        }
        // 先停大厅服
        try {
            SINGLETONS.instance(LobbyServer.class).stopServer();
        } catch (Exception e) {
            LOGGER.error("lobby server shutdown error", e);
        }
        // 销毁所有Manager
        MODULES.destroyAll();
    }
}
