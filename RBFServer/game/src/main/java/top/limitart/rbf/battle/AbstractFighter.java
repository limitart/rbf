package top.limitart.rbf.battle;


import top.limitart.concurrent.TaskQueue;
import top.limitart.concurrent.TaskQueueActor;

/**
 * 抽象战士
 * Created by Hank on 2018/11/2
 */
public abstract class AbstractFighter extends TaskQueueActor<BattleField> implements Fighter {
    @Override
    public TaskQueue res() {
        return where().res();
    }
}
