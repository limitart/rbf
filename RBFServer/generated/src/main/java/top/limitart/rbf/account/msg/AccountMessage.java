/*
 * Copyright (c) 2016-present The Limitart Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package top.limitart.rbf.account.msg;

import top.limitart.net.binary.*;

/**
 * 账户模块
 * 
 * @author limitart
 *
 */
public class AccountMessage {
	private static final byte PACKAGE_ID = 1;

   	/**
	 * 我的玩家信息
	 *
	 * @author limitart
	 *
	 */
	public static class MyPlayerInfo extends BinaryMeta {

		/**
		 * 服务器当前时间戳
		 */
		public long serverTime;
		/**
		 * 账户唯一ID
		 */
		public long uniqueID;
		/**
		 * 玩家名字
		 */
		public String name;
		/**
		 * 玩家头像信息
		 */
		public int avatar;
	}
   	/**
	 * 静态数据信息
	 *
	 * @author limitart
	 *
	 */
	public static class DataInfo extends BinaryMeta {

		/**
		 * 静态数据名称
		 */
		public String dataName;
		/**
		 * 静态数据二进制内容
		 */
		public byte[] dataBin;
	}
   	/**
	 * 静态数据列表
	 *
	 * @author limitart
	 *
	 */
	public static class DataSets extends BinaryMeta {

		/**
		 * 数据二进制列表
		 */
		public java.util.List<DataInfo> datas = new java.util.ArrayList<>();
	}

	/**
	 * 登陆失败
	 *
	 * @author limitart
	 *
	 */
	public static class ResLoginFail extends BinaryByteMessage {

		/**
		 * 用户名为空或超过32个长度
		 */
		public static final ResLoginFail USERNAME_EMPTY_OR_GREATER_THAN_32 = new ResLoginFail((byte)0);
		/**
		 * 用户名不能为中文
		 */
		public static final ResLoginFail NOT_CHINESE = new ResLoginFail((byte)1);
		/**
		 * 用户名包敏感字
		 */
		public static final ResLoginFail USERNAME_ILEAGLE = new ResLoginFail((byte)2);
		/**
		 * 重复登录
		 */
		public static final ResLoginFail LOGIN_AGAIN = new ResLoginFail((byte)3);
		/**
		 * 被顶号
		 */
		public static final ResLoginFail BE_LOGINED = new ResLoginFail((byte)4);
		/**
		 * 登录失败
		 */
		public static final ResLoginFail LOGIN_FAILED = new ResLoginFail((byte)5);

        public ResLoginFail() {
        }

        public ResLoginFail(byte code) {
            super(code);
        }

		@Override
		public short id() {
			return BinaryMessages.createID(PACKAGE_ID, 9);
		}

	}
	/**
	 * 创建玩家失败
	 *
	 * @author limitart
	 *
	 */
	public static class ResCreateMyPlayerFail extends BinaryByteMessage {

		/**
		 * 找不到帐号
		 */
		public static final ResCreateMyPlayerFail NO_ACCOUNT = new ResCreateMyPlayerFail((byte)0);
		/**
		 * 帐号不属于你
		 */
		public static final ResCreateMyPlayerFail ACCOUNT_NOT_BELONG_TO_YOU = new ResCreateMyPlayerFail((byte)1);
		/**
		 * 该帐号并未登录
		 */
		public static final ResCreateMyPlayerFail ACCOUNT_NOT_LOGIN = new ResCreateMyPlayerFail((byte)2);
		/**
		 * 已经创建过玩家了
		 */
		public static final ResCreateMyPlayerFail ALREADY_CREATED = new ResCreateMyPlayerFail((byte)3);
		/**
		 * 玩家名称过长
		 */
		public static final ResCreateMyPlayerFail NAME_TOO_LONG = new ResCreateMyPlayerFail((byte)4);
		/**
		 * 玩家名称重复
		 */
		public static final ResCreateMyPlayerFail NAME_DUPLICATED = new ResCreateMyPlayerFail((byte)5);
		/**
		 * 玩家名称存在敏感字
		 */
		public static final ResCreateMyPlayerFail NAME_ILEAGLE = new ResCreateMyPlayerFail((byte)6);
		/**
		 * 头像不存在
		 */
		public static final ResCreateMyPlayerFail AVATAR_NOT_EXIST = new ResCreateMyPlayerFail((byte)7);

        public ResCreateMyPlayerFail() {
        }

        public ResCreateMyPlayerFail(byte code) {
            super(code);
        }

		@Override
		public short id() {
			return BinaryMessages.createID(PACKAGE_ID, 17);
		}

	}

	/**
	 * 登录
	 *
	 * @author limitart
	 *
	 */
	public static class ReqLoginInner extends BinaryMessage {

		/**
		 * 帐号
		 */
		public String username;

		@Override
		public short id() {
			return BinaryMessages.createID(PACKAGE_ID, 7);
		}

	}
	/**
	 * 登录成功后发送我的玩家信息
	 *
	 * @author limitart
	 *
	 */
	public static class ResMyPlayerInfo extends BinaryMessage {

		/**
		 * 我的玩家信息(如果为空则玩家要创建新的角色)
		 */
		public MyPlayerInfo myPlayerInfo;

		@Override
		public short id() {
			return BinaryMessages.createID(PACKAGE_ID, 11);
		}

	}
	/**
	 * 创建我的玩家
	 *
	 * @author limitart
	 *
	 */
	public static class ReqCreateMyPlayer extends BinaryMessage {

		/**
		 * 角色名称
		 */
		public String name;
		/**
		 * 角色头像
		 */
		public int avatar;

		@Override
		public short id() {
			return BinaryMessages.createID(PACKAGE_ID, 13);
		}

	}
	/**
	 * 创建我的玩家成功
	 *
	 * @author limitart
	 *
	 */
	public static class ResCreateMyPlayerSuccess extends BinaryMessage {

		/**
		 * 我的玩家信息(如果为空则玩家要创建新的角色)
		 */
		public MyPlayerInfo myPlayerInfo;

		@Override
		public short id() {
			return BinaryMessages.createID(PACKAGE_ID, 15);
		}

	}
	/**
	 * 向服务器发送心跳
	 *
	 * @author limitart
	 *
	 */
	public static class ReqAccountHeart extends BinaryMessage {


		@Override
		public short id() {
			return BinaryMessages.createID(PACKAGE_ID, 19);
		}

	}
	/**
	 * 服务器返回心跳
	 *
	 * @author limitart
	 *
	 */
	public static class ResAccountHeart extends BinaryMessage {

		/**
		 * 服务器当前时间戳
		 */
		public long serverTime;

		@Override
		public short id() {
			return BinaryMessages.createID(PACKAGE_ID, 21);
		}

	}
	/**
	 * 向服务器发送心跳
	 *
	 * @author limitart
	 *
	 */
	public static class ResDataSet extends BinaryMessage {

		/**
		 * 静态数据
		 */
		public byte[] data;

		@Override
		public short id() {
			return BinaryMessages.createID(PACKAGE_ID, 23);
		}

	}
}
