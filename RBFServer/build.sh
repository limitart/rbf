#!/bin/bash
BUILD_PATH="./output"
#todo check

#build
rm -rf ${BUILD_PATH}
echo "start build project..."
mvn clean package -pl generated,game
if [[ ! -d ${BUILD_PATH} ]]; then
  mkdir ${BUILD_PATH}
fi
echo "copy game.jar..."
cp -rf ./game/target/game-1.0-SNAPSHOT.jar ${BUILD_PATH}/game.jar
echo "copy lib..."
cp -rf ./game/target/lib ${BUILD_PATH}/
echo "copy configs..."
cp -rf ./game/src/main/resources/server-config.properties ${BUILD_PATH}/
cp -rf ./game/src/main/resources/tables.sql ${BUILD_PATH}/
echo "copy windows start script 'start.bat...'"
cp -rf ./start.bat ${BUILD_PATH}/
echo "copy linux start script 'service.sh'"
cp -rf ./service.sh ${BUILD_PATH}/
echo "build done"
