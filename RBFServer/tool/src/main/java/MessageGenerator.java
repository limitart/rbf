import freemarker.template.Configuration;
import freemarker.template.Template;
import freemarker.template.TemplateException;
import org.xml.sax.SAXException;
import top.limitart.net.binary.BinaryMessageProtoFileInfo;
import top.limitart.net.binary.BinaryMessages;
import top.limitart.util.FileUtil;

import javax.xml.parsers.ParserConfigurationException;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.net.URISyntaxException;
import java.util.List;

/** 消息文件生成器 Created by Hank on 2018/9/4 */
public class MessageGenerator {
  private static Configuration cfg;
  private static Template template;

  public static void main(String[] args)
      throws URISyntaxException, SAXException, ParserConfigurationException, TemplateException,
          IOException {
    generateJava(Class.class.getResource("/msg").getPath(), "./generated/src/main");
  }

  public static void generateJava(String source, String out)
      throws IOException, SAXException, ParserConfigurationException, URISyntaxException,
          TemplateException {
    File file = new File(source);
    List<File> files = FileUtil.getFiles(file, "xml");
    for (int i = 0; i < files.size(); i++) {
      File f = files.get(i);
      BinaryMessageProtoFileInfo info =
          BinaryMessages.readBinaryMessageProtoFile((byte) (i + 1), f);
      BinaryMessages.generateMessageJava(info, out + "/java");
      for (BinaryMessageProtoFileInfo.BinaryMessageProtoInfo message : info.get_messages()) {
        for (BinaryMessageProtoFileInfo.BinaryMessageFieldProtoInfo field : message.get_fields()) {
          convertToCSType(field);
        }
      }
      for (BinaryMessageProtoFileInfo.BinaryMessageMetaProtoInfo message : info.get_metas()) {
        for (BinaryMessageProtoFileInfo.BinaryMessageFieldProtoInfo field : message.get_fields()) {
          convertToCSType(field);
        }
      }
      generateMessageCS(info, out + "/csharp");
      System.out.println(f.getPath());
    }
  }

  private static void convertToCSType(BinaryMessageProtoFileInfo.BinaryMessageFieldProtoInfo info) {
    if (info.get_type().equals("String")) {
      info.set_type("string");
    } else if (info.get_type().equals("boolean")) {
      info.set_type("bool");
    }
  }

  public static void generateMessageCS(BinaryMessageProtoFileInfo info, String outPath)
      throws URISyntaxException, IOException, TemplateException {
    if (cfg == null) {
      cfg = new Configuration();
      cfg.setDirectoryForTemplateLoading(new File(Class.class.getResource("/").toURI()));
      template = cfg.getTemplate("message_csharp.ftl");
    }
    File outDir = new File(outPath + File.separator);
    if (!outDir.exists()) {
      outDir.mkdirs();
    } else if (!outDir.isDirectory()) {
      throw new IOException("need a directory!");
    }
    try (FileWriter writer =
        new FileWriter(new File(outDir.getPath() + File.separator + info.get_name() + ".cs"))) {
      template.process(info, writer);
    }
  }
}
