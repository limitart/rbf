/*
 *
 *  * Copyright (c) 2016-present The Limitart Project
 *  *
 *  * Licensed under the Apache License, Version 2.0 (the "License");
 *  * you may not use this file except in compliance with the License.
 *  * You may obtain a copy of the License at
 *  *
 *  *   http://www.apache.org/licenses/LICENSE-2.0
 *  *
 *  * Unless required by applicable law or agreed to in writing, software
 *  * distributed under the License is distributed on an "AS IS" BASIS,
 *  * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  * See the License for the specific language governing permissions and
 *  * limitations under the License.
 *
 */

import freemarker.template.TemplateException;
import org.xml.sax.SAXException;
import top.limitart.dat.DatProtoFileInfo;
import top.limitart.dat.Dats;
import top.limitart.util.FileUtil;

import javax.xml.parsers.ParserConfigurationException;
import java.io.File;
import java.io.IOException;
import java.net.URISyntaxException;
import java.util.List;

/**
 * 静态数据Bin文件生成器
 *
 * @author hank
 * @version 2018/9/11 0011 1:25
 */
public class DatBinGenerator {
  public static void main(String[] args)
      throws URISyntaxException, SAXException, ParserConfigurationException, TemplateException,
          IOException {
    generate(Class.class.getResource("/dat").getPath(), "./dat");
  }

  public static void generate(String source, String out)
      throws IOException, SAXException, ParserConfigurationException, URISyntaxException,
          TemplateException {
    File file = new File(source);
    List<File> files = FileUtil.getFiles(file, "xlsx", "xls");
    for (File f : files) {
      Dats.generateDatBin(f, out);
      System.out.println(f.getPath());
    }
  }
}
