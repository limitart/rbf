CREATE TABLE IF NOT EXISTS `account` (
  `id` BIGINT NOT NULL PRIMARY KEY,
  `username` VARCHAR(32) NOT NULL,
  `create_time` BIGINT NOT NULL,
  `data` MEDIUMBLOB,

  UNIQUE KEY `index1`(`username`)
) ENGINE = innodb DEFAULT CHARSET = utf8;

CREATE TABLE IF NOT EXISTS `recharge` (
  `username` VARCHAR(32) NOT NULL,
  `gold` INT NOT NULL,
  `rmb` INT NOT NULL,
  `type` INT NOT NULL,
  `order` VARCHAR(255) NOT NULL PRIMARY KEY
) ENGINE = innodb DEFAULT CHARSET = utf8;

CREATE TABLE IF NOT EXISTS `server_global` (
  `id` INT NOT NULL,
  `dat` longtext NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;